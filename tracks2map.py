import pandas as pd
import numpy as np
import json
import matplotlib.pyplot as plt

'''
output:
np.array(time, dim, x, y))
'''
PROJECT_ID = "NarrVisAI"
VERSION = "baseline"

MAX_TICK_IN_GAME = 20000

# TODO change input path
NARRVIS_PATH = "/Users/thomsonwong/UniWork/Year2Sem1/Research Project/Sample Result"

# TODO change output path
OUT_PATH = "/Users/thomsonwong/UniWork/Year2Sem1/target/__meter"

# TODO change coordinate x, y
# cannot not be larger than MAP_DIFF
# set to 1,1 will get the total number
MAP_X = 1
MAP_Y = 1

VERBOSE = 0
TOTAL_NUMBER_OF_SIMULATION = 6500
LOG_INTERVAL = 3 # raw sampling interval
TIME_ECLAPSE = 40 # sample data for every X tick
TIME_SERIES_LENGTH = 13000 # maximum tick time for sampling in single series

# color_red = int(255/50)
# color_blue = int(255/24)

MAP_BOUND = [8, MAP_X, MAP_Y] #red unit, red fired, red jam, red track, blue unit, blue fired, blue jam, blue track

MAP_INDEX_RED_COOR = 0
MAP_INDEX_RED_FIRED = 1
MAP_INDEX_RED_JAMMED = 2
MAP_INDEX_RED_TRACKING = 3

MAP_INDEX_BLUE_COOR = 4
MAP_INDEX_BLUE_FIRED = 5
MAP_INDEX_BLUE_JAMMED = 6
MAP_INDEX_BLUE_TRACKING = 7

DEEP_MAP_BOUND  = [38, MAP_X, MAP_Y]

# units
DEEP_MAP_INDEX_RED_JET_COOR = 0
DEEP_MAP_INDEX_RED_GDAD_COOR = 1
DEEP_MAP_INDEX_RED_TANK_COOR =2
DEEP_MAP_INDEX_RED_DDG_COOR =3
DEEP_MAP_INDEX_RED_SUB_COOR =4
DEEP_MAP_INDEX_RED_AEWC_COOR = 5

DEEP_MAP_INDEX_BLUE_JET_COOR=6
DEEP_MAP_INDEX_BLUE_GDAD_COOR=7
DEEP_MAP_INDEX_BLUE_TANK_COOR=8
DEEP_MAP_INDEX_BLUE_DDG_COOR=9
DEEP_MAP_INDEX_BLUE_SUB_COOR=10
DEEP_MAP_INDEX_BLUE_AEWC_COOR = 11

# air projectile
DEEP_MAP_INDEX_RED_FIRED_AIM9=12
DEEP_MAP_INDEX_RED_FIRED_AIM20=13
DEEP_MAP_INDEX_RED_FIRED_AGM88=14
DEEP_MAP_INDEX_BLUE_FIRED_AIM9=15
DEEP_MAP_INDEX_BLUE_FIRED_AIM20=16
DEEP_MAP_INDEX_BLUE_FIRED_AGM88=17

# water projectile
DEEP_MAP_INDEX_RED_FIRED_TORPEDO=18
DEEP_MAP_INDEX_RED_SM2=19
DEEP_MAP_INDEX_BLUE_FIRED_TORPEDO=20
DEEP_MAP_INDEX_BLUE_SM2=21

# land projectile
DEEP_MAP_INDEX_RED_FIRED_CANNON=22
DEEP_MAP_INDEX_RED_FIRED_SAM=23
DEEP_MAP_INDEX_BLUE_FIRED_CANNON=24
DEEP_MAP_INDEX_BLUE_FIRED_SAM=25

# jamming
DEEP_MAP_INDEX_RED_JAMMED_AIR=26
DEEP_MAP_INDEX_RED_JAMMED_GROUND=27
DEEP_MAP_INDEX_RED_JAMMED_SURFACE=28
DEEP_MAP_INDEX_BLUE_JAMMED_AIR=29
DEEP_MAP_INDEX_BLUE_JAMMED_GROUND=30
DEEP_MAP_INDEX_BLUE_JAMMED_SURFACE=31

# tracking
DEEP_MAP_INDEX_RED_TRACKING_AIR=32
DEEP_MAP_INDEX_RED_TRACKING_GROUND=33
DEEP_MAP_INDEX_RED_TRACKING_SURFACE=34
DEEP_MAP_INDEX_BLUE_TRACKING_AIR=35
DEEP_MAP_INDEX_BLUE_TRACKING_GROUND=36
DEEP_MAP_INDEX_BLUE_TRACKING_SURFACE=37

MAP_MIN_X = 109.0
MAP_MAX_X = 157.0
MAP_MIN_Y = -28.045454545454547
MAP_MAX_Y = 1.0454545454545467

MAP_DIFF_X = MAP_MAX_X - MAP_MIN_X
MAP_DIFF_Y = MAP_MAX_Y - MAP_MIN_Y

MAP_UNIT_LENGTH_X = MAP_DIFF_X / float(MAP_X)
MAP_UNIT_LENGTH_Y = MAP_DIFF_Y / float(MAP_Y)

COL_TRACKS = 'tracks'
COL_TIME = 'time'
COL_COOR = 'coordinates'

UNIT_JET = 'jet'
UNIT_DDG = 'DDG'
UNIT_TANK = "tank"
UNIT_SUB = "sub"
UNIT_GDAD = "GBAD"
UNIT_AEWC = "AEW&C"

FIRED_AIM120 = "AIM120"
FIRED_AIM9 = "AIM9"
FIRED_AGM88 = "AGM88"
FIRED_SM2 = "SM2"
FIRED_CANNON = "cannon"
FIRED_SAM = "SAM"
FIRED_TORPEDO = "torpedo"


FORCE_BLUE = 'blue'
FORCE_RED = 'red'

# range 0 to (lenath -1)
def convert2positiveCoordinate(coordinate):
  return [ 
    int(round(( float(coordinate[0]) - MAP_MIN_X) / MAP_UNIT_LENGTH_X, 0)) - 1,
    int(round(( float(coordinate[1]) - MAP_MIN_Y) / MAP_UNIT_LENGTH_Y, 0)) - 1
   ]

def convert(case_id):
  path = f"{NARRVIS_PATH}/{case_id}/jforce_v6a-tracks.json"
  event_path = f"{NARRVIS_PATH}/{case_id}/jforce_v6a-allevents.json"
  data = None
  events = None
  maps = list()
  deep_maps = list()
  time_arr = list()
  
  # unit coordinate extraction
  with open(path) as json_file:
    data = json.load(json_file)
  data[COL_TRACKS]
  result = pd.DataFrame(columns=["time", "map"])
  count = 0
  for row in data[COL_TRACKS]:
    my_map = np.zeros(MAP_BOUND)
    maps.append(my_map)

    my_deep_map =np.zeros(DEEP_MAP_BOUND)
    deep_maps.append(my_deep_map)
    
    time = row[COL_TIME] # index * LOG_INTERVAL = time
    time_arr.append(time)

    coors = row['coordinates']
    for unit in coors:
      deep_index = None
      if UNIT_JET in unit:
        deep_index = DEEP_MAP_INDEX_RED_JET_COOR if FORCE_RED in unit else DEEP_MAP_INDEX_BLUE_JET_COOR
      elif UNIT_DDG in unit:
        deep_index = DEEP_MAP_INDEX_RED_DDG_COOR if FORCE_RED in unit else DEEP_MAP_INDEX_BLUE_DDG_COOR
      elif UNIT_SUB in unit:
        deep_index = DEEP_MAP_INDEX_BLUE_SUB_COOR if FORCE_RED in unit else DEEP_MAP_INDEX_RED_SUB_COOR
      elif UNIT_TANK in unit:
        deep_index = DEEP_MAP_INDEX_RED_TANK_COOR if FORCE_RED in unit else DEEP_MAP_INDEX_BLUE_TANK_COOR
      elif UNIT_GDAD in unit:
        deep_index = DEEP_MAP_INDEX_RED_GDAD_COOR if FORCE_RED in unit else DEEP_MAP_INDEX_BLUE_GDAD_COOR
      elif UNIT_AEWC in unit:
        deep_index = DEEP_MAP_INDEX_RED_AEWC_COOR if FORCE_RED in unit else DEEP_MAP_INDEX_BLUE_AEWC_COOR
      else:
        print("coor " + unit + " error")
      index = MAP_INDEX_RED_COOR if FORCE_RED in unit else MAP_INDEX_BLUE_COOR
      coor = convert2positiveCoordinate(coors[unit])
      my_map[index][coor[0]][coor[1]] = my_map[index][coor[0]][coor[1]] + 1
      my_deep_map[deep_index][coor[0]][coor[1]] = my_deep_map[deep_index][coor[0]][coor[1]] + 1

  # plot heatmap
  # coor_map = my_map[[MAP_INDEX_RED_COOR, MAP_INDEX_BLUE_COOR]]
  #   if count % 120 == 0:
  #     fig = plt.figure(figsize=(15,8))
        # fig.add_subplot(1, 2, 2)
        # plt.imshow( my_map )

        # plt.title(f"time={count}")
        # plt.colorbar()
        # plt.show()
        # print("END")
  #   count += 1

  # handle other events
  with open(event_path) as json_file:
    events = json.load(json_file)

  events = events['events']
  for row in events:
    if "fired" in row[-1]:
      time = row[1] # index * LOG_INTERVAL = time
      units = row[3] # UnitA, UnitB "type(force,ID)"
      index = int(time / LOG_INTERVAL)
      ele = row[4] # action, coorA, coorB

      deep_force = None
      if FIRED_AIM120 in ele[0]:
        deep_force = DEEP_MAP_INDEX_RED_FIRED_AIM20 if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_FIRED_AIM20
      elif FIRED_AIM9 in ele[0]:
        deep_force = DEEP_MAP_INDEX_RED_FIRED_AIM9 if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_FIRED_AIM9
      elif FIRED_AGM88 in ele[0]:
        deep_force = DEEP_MAP_INDEX_RED_FIRED_AGM88 if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_FIRED_AGM88
      elif FIRED_SM2 in ele[0]:
        deep_force = DEEP_MAP_INDEX_RED_SM2 if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_SM2
      elif FIRED_TORPEDO in ele[0]:
        deep_force = DEEP_MAP_INDEX_RED_FIRED_TORPEDO if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_FIRED_TORPEDO
      elif FIRED_CANNON in ele[0]:
        deep_force = DEEP_MAP_INDEX_RED_FIRED_CANNON if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_FIRED_CANNON
      elif FIRED_SAM in ele[0]:
        deep_force = DEEP_MAP_INDEX_RED_FIRED_SAM if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_FIRED_SAM
      else:
        print("fire," + ele[0] + " " + units[0] + " error")

      force = MAP_INDEX_RED_FIRED if "red" in units[0] else MAP_INDEX_BLUE_FIRED
      coorA = ele[1]
      coorA = convert2positiveCoordinate(coorA)

      maps[index][force][coorA[0]][coorA[1]] = maps[index][force][coorA[0]][coorA[1]] + 1
      deep_maps[index][deep_force][coorA[0]][coorA[1]] = deep_maps[index][deep_force][coorA[0]][coorA[1]] + 1
      
    elif "killed" in row[-1]:
      continue
    elif "start" in row[-1]:
      continue
    elif "begins.tracking" in row[-1]:
      continue
    elif "ends.tracking" in row[-1]:
       continue
    elif "tracking" in row[-1]:
      time = row[1] # index * LOG_INTERVAL = time
      units = row[3] # UnitA(tracker), UnitB(trackee) "type(force,ID)"
      index = int(time / LOG_INTERVAL)
      ele = row[4] # action, coorA, coorB

      deep_force = None
      if UNIT_JET in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_TRACKING_AIR if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_TRACKING_AIR
      elif UNIT_AEWC in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_TRACKING_AIR if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_TRACKING_AIR
      elif UNIT_DDG in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_TRACKING_SURFACE if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_TRACKING_SURFACE
      elif UNIT_SUB in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_TRACKING_SURFACE if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_TRACKING_SURFACE
      elif UNIT_TANK in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_TRACKING_GROUND if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_TRACKING_GROUND
      elif UNIT_GDAD in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_TRACKING_GROUND if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_TRACKING_GROUND
      else:
        print( "tracking," + units[0] +" error")

      force = MAP_INDEX_RED_TRACKING if "red" in units[0] else MAP_INDEX_BLUE_TRACKING
      coorA = ele[1]
      coorA = convert2positiveCoordinate(coorA)
      maps[index][force][coorA[0]][coorA[1]] = maps[index][force][coorA[0]][coorA[1]] + 1
      deep_maps[index][deep_force][coorA[0]][coorA[1]] = deep_maps[index][deep_force][coorA[0]][coorA[1]] + 1

    elif "begins.jamming" in row[-1]:
      continue
    elif "ends.jamming" in row[-1]:
      continue
    elif "jamming" in row[-1]:
      time = row[1] # index * LOG_INTERVAL = time
      units = row[3] # UnitA, UnitB "type(force,ID)"
      index = int(time / LOG_INTERVAL)
      ele = row[4] # action, coorA, coorB

      deep_force = None
      if UNIT_JET in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_JAMMED_AIR if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_JAMMED_AIR
      elif UNIT_AEWC in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_JAMMED_AIR if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_JAMMED_AIR
      elif UNIT_DDG in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_JAMMED_SURFACE if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_JAMMED_SURFACE
      elif UNIT_SUB in UNIT_SUB[0]:
        deep_force = DEEP_MAP_INDEX_RED_JAMMED_SURFACE if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_JAMMED_SURFACE
      elif UNIT_TANK in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_JAMMED_GROUND if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_JAMMED_GROUND
      elif UNIT_GDAD in units[0]:
        deep_force = DEEP_MAP_INDEX_RED_JAMMED_GROUND if FORCE_RED in units[0] else DEEP_MAP_INDEX_BLUE_JAMMED_GROUND
      else:
        print("jamming," + units[0] + "error")

      force = MAP_INDEX_RED_JAMMED if "red" in units[0] else MAP_INDEX_BLUE_JAMMED
      coorA = ele[1]
      coorA = convert2positiveCoordinate(coorA)
      maps[index][force][coorA[0]][coorA[1]] = maps[index][force][coorA[0]][coorA[1]] + 1
      deep_maps[index][deep_force][coorA[0]][coorA[1]] = deep_maps[index][deep_force][coorA[0]][coorA[1]] + 1
    else:
      print(row[-1])
  return np.stack(maps, axis=0), np.stack(deep_maps, axis=0)

# main
# TODO update #id of run 
for i in range(0, 5000):
  maps, deep_maps = convert(i)
  np.save(f'map_{i}.npy', maps)
  np.save(f'deep_map_{i}.npy', maps)

# maps = np.load('test_map_2.npy')
# batch_size = 4
# input_shape = np.append(MAP_BOUND, 1)
# input_img = layers.Input(shape=(input_shape))
# x = layers.Conv3D(128, (3, 3, 4), activation='relu', padding='same')(input_img)
# # x = layers.MaxPooling3D((2, 2, 1), padding='same')(x)
# x = layers.Conv3D(64, (3, 3, 4), activation='relu', padding='same')(x)
# # x = layers.MaxPooling3D((2, 2, 1), padding='same')(x)
# x = layers.Conv3D(32, (3, 3, 4), activation='relu', padding='same')(x)
# encoded = layers.MaxPooling3D((2, 2, 1), padding='same')(x)

# # at this point the representation is (4, 4, 8) i.e. 128-dimensional

# x = layers.Conv3D(32, (3, 3, 4), activation='relu', padding='same')(encoded)
# x = layers.UpSampling3D((2, 2, 1))(x)
# x = layers.Conv3D(64, (3, 3, 4), activation='relu', padding='same')(x)
# # x = layers.UpSampling3D((2, 2, 1))(x)
# x = layers.Conv3D(128, (3, 3, 4), activation='relu', padding='same')(x)
# # x = layers.UpSampling3D((2, 2, 1))(x)
# decoded = layers.Conv3D(1, (3, 3, 4), activation='sigmoid', padding='same')(x)

# autoencoder = Model(input_img, decoded)
# autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
# print(autoencoder.summary())

# x_train = maps[:int(len(maps)*0.8)]
# x_test= maps[int(len(maps)*0.8):]
# # x_train = np.reshape(x_train, (1, x_train.shape[0], x_train.shape[1], x_train.shape[2], x_train.shape[3]))
# # x_test = np.reshape(x_test, (1, x_test.shape[0], x_test.shape[1], x_test.shape[2], x_test.shape[3]))
# print(x_test.shape)
# autoencoder.fit(x_train, x_train,
#                 epochs=50,
#                 batch_size=128,
#                 shuffle=False,
#                 validation_data=(x_test, x_test),
#                 callbacks=[TensorBoard(log_dir='/tmp/autoencoder')])
# #model_ed.add(layers.Conv3D(filter=4, kernel_size=(2,2,4), input_shape=input_shape))
