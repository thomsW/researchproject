import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
from datetime import date
from datetime import datetime
from datetime import timedelta
from time import mktime
import time
import math as math
import shapefile as shp
from jsonc_parser.parser import JsoncParser
# %matplotlib inline

SIM_ID = 2

SHAPE_FILE = "/Users/thomsonwong/UniWork/Year2Sem1/target/theUpsidedown2.shp"
MAP_BOUNDS = [109.0, 157.0, -28.045454545454547, 1.0454545454545467]
PATCH_BOUNDS = [0.0, 329.0, 0.0, 199.0]
start = 1500
end = 2000

sim = JsoncParser.parse_file("/Users/thomsonwong/UniWork/Year2Sem1/Research Project/Sample Result/2/jforce_v6a-tracks.json")
unit_dict = {}
first_row = True
for row in sim['tracks']:
  unit_coors = row['coordinates']
  for unit, coor in unit_coors.items():
    if first_row:
      unit_dict[unit] = np.array(coor)
    else :
      unit_dict[unit] = np.vstack( (unit_dict[unit], coor) )
  first_row = False

for unit in unit_dict.keys():
  unit_dict[unit] = unit_dict[unit].T



# plot track
fig = plt.figure()

axes = fig.add_axes([0, 0, 2, 2])
axes.set_title('Tracks for run %s' % SIM_ID)
axes.set_xlabel('X')
axes.set_ylabel('Y')
axes.grid(True)
axes.set_xlim(MAP_BOUNDS[0], MAP_BOUNDS[1])
axes.set_ylim(MAP_BOUNDS[2], MAP_BOUNDS[3])

sf = shp.Reader(SHAPE_FILE)
for shape in sf.shapeRecords():
    x = [i[0] for i in shape.shape.points[:]]
    y = [i[1] for i in shape.shape.points[:]]
    plt.plot(x, y)

for unit in unit_dict.keys():
    coor = unit_dict[unit]
    atype, acolor, aid = unit.replace('(', ',').replace(')', '').split(',')
    if atype == 'jet' and len(coor[0]) > start:
        alabel = '%s (%s)' % (aid, atype)
        axes.plot(coor[0][start: min(len(coor[0]), end)], coor[1][start: min(len(coor[1]), end)], label=alabel, color=acolor)

# plt.legend()
plt.savefig("out.png", bbox_inches = 'tight')
# plt.show()
