import os
import copy
import json

# TODO change path
OUTPUT_PATH = "/data/data1/kalong/NarrVis/June2021/data"
# TODO change loop range - simulation run index
RANGE = [0,5000]
# TODO change raw simulation directory
INPUT_PATH = "/data/data1/kalong/simulations/"

# SHAPE_FILE = os.path.expanduser("~/yuan/NarrVis/jforce/nv-jforce/theUpsidedown2.shp")

MAP_BOUNDS = [109.0, 157.0, -28.045454545454547, 1.0454545454545467]
PATCH_BOUNDS = [0.0,329.0,0.0,199.0]

# model parameters
PATCH_PER_KM = 0.06182836354225495
TICKS_PER_MIN = 20.0
TICKS_PER_HOUR = TICKS_PER_MIN * 60.0

# set up cases where not all expected model parameters are defined.
INDEX_NUM_BLUE_JETS = None
INDEX_BLUE_JET_SPEED = None
INDEX_NUM_BLUE_GBADS = None
INDEX_BLUE_GBAD_SENSOR_RANGE = None
INDEX_NUM_BLUE_DDG = None
INDEX_BLUE_DDG_SENSOR_RANGE = None
INDEX_BLUE_GBAD_WEAPON_COUNT = None

BLUE_GBAD_WEAPON_COUNT = 0
BLUE_GBAD_SENSOR_RANGE = 120.0
BLUE_SM2_COUNT = 48
BLUE_JET_SPEED = 2000.0
BLUE_DDG_SENSOR_RANGE = 300.0
BLUE_SUB_SENSOR_RANGE = 170.0

# indices to access model parameters
INDEX_RUN_NUMBER = 0
INDEX_COMMS_RANGE = 3
INDEX_BLUE_CEC = 4
INDEX_RED_CEC = 5
INDEX_BLUE_JET_SENSOR_RANGE = 6
INDEX_RED_JET_SENSOR_RANGE = 7
#INDEX_NUM_BLUE_JETS = 8
INDEX_NUM_RED_JETS = 8
#INDEX_BLUE_JET_SPEED = 10
INDEX_RED_JET_SPEED = 9
INDEX_BLUE_AEWC_JAMMING = 10
INDEX_RED_AEWC_JAMMING = 11
INDEX_BLUE_AEWC_JAM_RANGE = 12
INDEX_RED_AEWC_JAM_RANGE = 13
INDEX_NUM_BLUE_AEWC = 14
INDEX_NUM_RED_AEWC = 15
INDEX_BLUE_AEWC_SPEED = 16
INDEX_RED_AEWC_SPEED = 17
INDEX_BLUE_AEWC_SENSOR_RANGE = 18
INDEX_RED_AEWC_SENSOR_RANGE = 19
INDEX_NUM_BLUE_TANKS = 20
INDEX_NUM_RED_TANKS = 21
INDEX_BLUE_TANK_SPEED = 22
INDEX_RED_TANK_SPEED = 23
INDEX_BLUE_TANK_SENSOR_RANGE = 24
INDEX_RED_TANK_SENSOR_RANGE = 25
INDEX_BLUE_TANK_WEAPON_RANGE = 26
INDEX_RED_TANK_WEAPON_RANGE = 27
#INDEX_NUM_BLUE_GBADS = 30
INDEX_NUM_RED_GBADS = 28
INDEX_BLUE_GBAD_SPEED = 29
INDEX_RED_GBAD_SPEED = 30
#INDEX_BLUE_GBAD_SENSOR_RANGE = 34
INDEX_RED_GBAD_SENSOR_RANGE = 31
INDEX_BLUE_GBAD_WEAPON_RANGE = 32
INDEX_RED_GBAD_WEAPON_RANGE = 33
#INDEX_NUM_BLUE_DDG = 38
INDEX_NUM_RED_DDG = 34
INDEX_BLUE_DDG_SPEED = 35
INDEX_RED_DDG_SPEED = 36
#INDEX_BLUE_DDG_SENSOR_RANGE = 42
INDEX_RED_DDG_SENSOR_RANGE = 37
INDEX_BLUE_DDG_WEAPON_RANGE = 38
INDEX_RED_DDG_WEAPON_RANGE = 39
INDEX_NUM_BLUE_HUMVEE = 40
INDEX_NUM_RED_HUMVEE = 41
INDEX_BLUE_HUMVEE_SPEED = 42
INDEX_RED_HUMVEE_SPEED = 43
INDEX_BLUE_HUMVEE_SENSOR_RANGE = 44
INDEX_RED_HUMVEE_SENSOR_RANGE = 45
INDEX_BLUE_HUMVEE_WEAPON_RANGE = 46
INDEX_RED_HUMVEE_WEAPON_RANGE = 47
INDEX_NUM_BLUE_SUBS = 48
INDEX_NUM_RED_SUBS = 49
INDEX_BLUE_SUB_SPEED = 50
INDEX_RED_SUB_SPEED = 51
INDEX_BLUE_SUB_SENSOR_RANGE = 52
INDEX_RED_SUB_SENSOR_RANGE = 53
INDEX_BLUE_SUB_WEAPON_RANGE = 54
INDEX_RED_SUB_WEAPON_RANGE = 55
INDEX_AGM88_RANGE = 56
INDEX_REDAIM120 = 57
INDEX_BLUEAIM120 = 58
INDEX_AIM120_PK = 59
INDEX_AIM120_RANGE = 60
INDEX_REDAIM9 = 61
INDEX_BLUEAIM9 = 62
INDEX_AIM9_PK = 63
INDEX_AIM9_RANGE = 64
INDEX_REDAGM88 = 65
INDEX_BLUEAGM88 = 66
INDEX_AGM88_PK = 67
INDEX_REDSM2_COUNT = 68
INDEX_BLUESM2_COUNT = 69
INDEX_DDG_PK = 70
#INDEX_BLUE_GBAD_WEAPON_COUNT = 77
INDEX_RED_GBAD_WEAPON_COUNT = 71

# indices to access netlogo data
TABLE_OFFSET = 97
INDEX_TICK = TABLE_OFFSET+0
INDEX_RED_TANKS = TABLE_OFFSET+1
INDEX_RED_JETS  = TABLE_OFFSET+2
INDEX_RED_DDGS  = TABLE_OFFSET+3
INDEX_RED_SUBS  = TABLE_OFFSET+4
INDEX_RED_GBADS = TABLE_OFFSET+5
INDEX_BLUE_TANKS = TABLE_OFFSET+6
INDEX_BLUE_JETS  = TABLE_OFFSET+7
INDEX_BLUE_DDGS  = TABLE_OFFSET+8
INDEX_BLUE_SUBS  = TABLE_OFFSET+9
INDEX_BLUE_GBADS = TABLE_OFFSET+10
INDEX_BLUE_DIST  = TABLE_OFFSET+11
INDEX_RED_DIST   = TABLE_OFFSET+12
INDEX_BLUE_AIM120 = TABLE_OFFSET+13
INDEX_BLUE_AIM9   = TABLE_OFFSET+14
INDEX_BLUE_AGM88  = TABLE_OFFSET+15
INDEX_RED_AIM120  = TABLE_OFFSET+16
INDEX_RED_AIM9    = TABLE_OFFSET+17 
INDEX_RED_AGM88   = TABLE_OFFSET+18
INDEX_SEED        = TABLE_OFFSET+19
INDEX_EVENTS      = TABLE_OFFSET+20
INDEX_LOCS        = TABLE_OFFSET+21

NONTRACK_TYPES = ['fire','CAPLoc','base']

# events that dont have an associated position but we should find it from previous track data
FINDTRACK_TYPES = ['killed']

eventSignificance = {
    'tracking': 1.0,
    'killed': 5.0,
    'default': 1.0
}

def tick2time(tick):
    return 60.0*float(tick)/TICKS_PER_MIN

def getEntityID(eid,etype,ecolor):
    return "%s(%s,%s)" % (etype,ecolor,eid)

# compile data array 
def getReports(data, indices):
    # ([ticks], [xcor], [ycor])
    t = []
    x = []
    for d in data:
        # get data
        tt = tick2time(d[INDEX_TICK])
        xx = []
        #print "DEBUG:: %s: %s" % (tt,d)
        for idx in indices:
            #print "DEBUG:: index: %s" % idx
            #print "DEBUG:: data:  %s" % d[idx]
            #xx.append(float(d[idx]))
            xx.append(int(d[idx]))
        
        # build list
        t.append(tt)
        x.append(xx)
        
    # x is a list of row data
    # now will want to transpose this into a list of column data
    m = [t]
    for i in range(len(indices)):
        c = []
        for xx in x:
            # add ith element to column
            c.append(xx[i])
        m.append(c)
    return m

# return dict with all tracks
def getAllTracks(data):
    tracks = []
    for d in data:
        time = tick2time(d[INDEX_TICK])
        locs = d[INDEX_LOCS]
        update = {"time": time, "coordinates":{}}
        for a in locs:
            if a["type"] not in NONTRACK_TYPES:
                aid = getEntityID(a["id"],a["type"],a["color"])
                if 'at' in a:
                    update["coordinates"][aid] = [float(a["at"][0]),float(a["at"][1])]
                 
        tracks.append(update)
    return {"tracks": tracks}

# return dict with all platforms and associated info
# updates at each time tick (but should be constant)
def getPlatforms(data):
    platforms = {}
    locations = {}
    units = {"speed": "km/hr", "range": "km", "coordinates": "lonlatdegs"}
    for d in data:
        locs = d[INDEX_LOCS]
        for a in locs:
            aid = getEntityID(a["id"],a["type"],a["color"])
            if not (a["type"] in ["fire", "base", "CAPLoc"]):
                platform = {"type": a["type"], "allegiance": a["color"]}
                if a["type"] == "jet":
                    if a["color"] == "red":
                        platform["speed"] = float(d[INDEX_RED_JET_SPEED])
                        platform["sensors"] = [{"sensor": "radar", "domains": ["air","ground","surface"], "range": float(d[INDEX_RED_JET_SENSOR_RANGE])}]
                        platform["jammers"] = []
                        platform["comms"] = [{"network": "link16", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])}]
                        if d[INDEX_RED_CEC] == "true":
                            platform["comms"].append({"network": "CEC", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])})
                        
                        platform["weapons"] = [{"weapon": "AIM9", "domains": ["air"], "load": int(d[INDEX_REDAIM9]), "range": float(d[INDEX_AIM9_RANGE]), "PK": float(d[INDEX_AIM9_PK])},
                                               {"weapon": "AIM120", "domains": ["air"], "load": int(d[INDEX_REDAIM120]), "range": float(d[INDEX_AIM120_RANGE]), "PK": float(d[INDEX_AIM120_PK])},
                                               {"weapon": "AGM88", "domains": ["ground"], "load": int(d[INDEX_REDAGM88]), "range": float(d[INDEX_AGM88_RANGE]), "PK": float(d[INDEX_AGM88_PK])}] 
                    else:
                        if INDEX_BLUE_JET_SPEED:
                            platform["speed"] = float(d[INDEX_BLUE_JET_SPEED])
                        else:
                            platform["speed"] = float(BLUE_JET_SPEED)
                        platform["sensors"] = [{"sensor": "radar", "domains": ["air","ground","surface"], "range": float(d[INDEX_BLUE_JET_SENSOR_RANGE])}]
                        platform["jammers"] = []
                        platform["comms"] = [{"network": "link16", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])}]
                        if d[INDEX_BLUE_CEC] == "true":
                            platform["comms"].append({"network": "CEC", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])})
                        platform["weapons"] = [{"weapon": "AIM9", "domains": ["air"], "load": int(d[INDEX_BLUEAIM9]), "range": float(d[INDEX_AIM9_RANGE]), "PK": float(d[INDEX_AIM9_PK])},
                                               {"weapon": "AIM120", "domains": ["air"], "load": int(d[INDEX_BLUEAIM120]), "range": float(d[INDEX_AIM120_RANGE]), "PK": float(d[INDEX_AIM120_PK])},
                                               {"weapon": "AGM88", "domains": ["ground"], "load": int(d[INDEX_BLUEAGM88]), "range": float(d[INDEX_AGM88_RANGE]), "PK": float(d[INDEX_AGM88_PK])}]

                elif a["type"] == "AEW&C":
                    if a["color"] == "red":
                        platform["speed"] = float(d[INDEX_RED_AEWC_SPEED])
                        platform["sensors"] = [{"sensor": "radar", "domains": ["air","ground","surface"], "range": float(d[INDEX_RED_AEWC_SENSOR_RANGE])}]
                        if d[INDEX_RED_AEWC_JAMMING] == "true":
                            platform["jammers"] = [{"jammer": "jammer", "domains": ["air","ground","surface"], "range": float(d[INDEX_RED_AEWC_JAM_RANGE])}]
                        else:
                            platform["jammers"] = []                            
                        platform["comms"] = [{"network": "link16", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])}]
                        if d[INDEX_RED_CEC] == "true":
                            platform["comms"].append({"network": "CEC", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])})
                    else:
                        platform["speed"] = float(d[INDEX_BLUE_AEWC_SPEED])
                        platform["sensors"] = [{"sensor": "radar", "domains": ["air","ground","surface"], "range": float(d[INDEX_BLUE_AEWC_SENSOR_RANGE])}]
                        if d[INDEX_BLUE_AEWC_JAMMING] == "true":
                            platform["jammers"] = [{"jammer": "jammer", "domains": ["air","ground","surface"], "range": float(d[INDEX_BLUE_AEWC_JAM_RANGE])}]
                        else:
                            platform["jammers"] = []                            
                        platform["comms"] = [{"network": "link16", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])}]
                        if d[INDEX_BLUE_CEC] == "true":
                            platform["comms"].append({"network": "CEC", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])})

                elif a["type"] == "DDG":
                    if a["color"] == "red":
                        platform["speed"] = float(d[INDEX_RED_DDG_SPEED])
                        platform["sensors"] = [{"sensor": "radar", "domains": ["air","surface"], "range": float(d[INDEX_RED_DDG_SENSOR_RANGE])}]
                        platform["weapons"] = [{"weapon": "SM2", "domains": ["air"], "load": int(d[INDEX_REDSM2_COUNT]), "range": float(d[INDEX_RED_DDG_WEAPON_RANGE]), "PK": float(d[INDEX_DDG_PK])}] 
                        platform["jammers"] = []
                        platform["comms"] = [{"network": "link16", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])}]
                        if d[INDEX_RED_CEC] == "true":
                            platform["comms"].append({"network": "CEC", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])})
                    else:
                        platform["speed"] = float(d[INDEX_BLUE_DDG_SPEED])
                        if INDEX_BLUE_DDG_SENSOR_RANGE:
                            platform["sensors"] = [{"sensor": "radar", "domains": ["air","surface"], "range": float(d[INDEX_BLUE_DDG_SENSOR_RANGE])}]
                        else:
                            platform["sensors"] = [{"sensor": "radar", "domains": ["air","surface"], "range": float(BLUE_DDG_SENSOR_RANGE)}]
                        platform["weapons"] = [{"weapon": "SM2", "domains": ["air"], "load": int(d[INDEX_BLUESM2_COUNT]), "range": float(d[INDEX_BLUE_DDG_WEAPON_RANGE]), "PK": float(d[INDEX_DDG_PK])}] 
                        platform["jammers"] = []
                        platform["comms"] = [{"network": "link16", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])}]
                        if d[INDEX_BLUE_CEC] == "true":
                            platform["comms"].append({"network": "CEC", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])})
                        
                elif a["type"] == "GBAD":
                    if a["color"] == "red":
                        platform["speed"] = float(d[INDEX_RED_GBAD_SPEED])
                        platform["sensors"] = [{"sensor": "radar", "domains": ["air"], "range": float(d[INDEX_RED_GBAD_SENSOR_RANGE])}]
                        platform["jammers"] = []
                        platform["weapons"] = [{"weapon": "SAM", "domains": ["air"], "load": int(d[INDEX_RED_GBAD_WEAPON_COUNT]), "range": float(d[INDEX_RED_GBAD_WEAPON_RANGE]), "PK": 1.0}] 
                        platform["comms"] = [{"network": "link16", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])}]
                        if d[INDEX_RED_CEC] == "true":
                            platform["comms"].append({"network": "CEC", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])})
                    else:
                        platform["speed"] = float(d[INDEX_BLUE_GBAD_SPEED])
                        if INDEX_BLUE_GBAD_SENSOR_RANGE:
                            platform["sensors"] = [{"sensor": "radar", "domains": ["air","surface"], "range": float(d[INDEX_BLUE_GBAD_SENSOR_RANGE])}]
                        else:
                            platform["sensors"] = [{"sensor": "radar", "domains": ["air","surface"], "range": float(BLUE_GBAD_SENSOR_RANGE)}]

                        platform["jammers"] = []
                        if INDEX_BLUE_GBAD_WEAPON_COUNT:
                            platform["weapons"] = [{"weapon": "SAM", "domains": ["air"], "load": int(d[INDEX_BLUE_GBAD_WEAPON_COUNT]), "range": float(d[INDEX_BLUE_GBAD_WEAPON_RANGE]), "PK": 1.0}] 
                        else:
                            platform["weapons"] = [{"weapon": "SAM", "domains": ["air"], "load": int(BLUE_GBAD_WEAPON_COUNT), "range": float(d[INDEX_BLUE_GBAD_WEAPON_RANGE]), "PK": 1.0}] 
                        platform["comms"] = [{"network": "link16", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])}]
                        if d[INDEX_BLUE_CEC] == "true":
                            platform["comms"].append({"network": "CEC", "domains": ["air","ground","surface"], "range": float(d[INDEX_COMMS_RANGE])})

                elif a["type"] == "sub":
                    if a["color"] == "red":
                        platform["speed"] = float(d[INDEX_RED_SUB_SPEED])
                        platform["sensors"] = [{"sensor": "sonar", "domains": ["subsurface","surface"], "range": float(d[INDEX_RED_SUB_SENSOR_RANGE])}]
                        platform["jammers"] = []
                        platform["comms"] = []
                        platform["weapons"] = [{"weapon": "torpedo", "domains": ["surface", "subsurface"], "load": "unlimited", "range": float(d[INDEX_RED_SUB_WEAPON_RANGE]), "PK": 1.0}] 
                    else:
                        platform["speed"] = float(d[INDEX_BLUE_SUB_SPEED])
                        if INDEX_BLUE_SUB_SENSOR_RANGE:
                            platform["sensors"] = [{"sensor": "sonar", "domains": ["subsurface","surface"], "range": float(d[INDEX_BLUE_SUB_SENSOR_RANGE])}]
                        else:
                            platform["sensors"] = [{"sensor": "sonar", "domains": ["subsurface","surface"], "range": float(BLUE_SUB_SENSOR_RANGE)}]
                        platform["jammers"] = []
                        platform["comms"] = []
                        platform["weapons"] = [{"weapon": "torpedo", "domains": ["surface", "subsurface"], "load": "unlimited", "range": float(d[INDEX_BLUE_SUB_WEAPON_RANGE]), "PK": 1.0}] 
                elif a["type"] == "tank":
                    if a["color"] == "red":
                        platform["speed"] = float(d[INDEX_RED_TANK_SPEED])
                        platform["weapons"] = [{"weapon": "cannon", "domains": ["ground"], "load": "unlimited", "range": float(d[INDEX_RED_TANK_WEAPON_RANGE]), "PK": 1.0}] 
                        platform["sensors"] = [{"sensor": "EO/IR", "domains": ["ground"], "range": float(d[INDEX_RED_TANK_SENSOR_RANGE])}]
                        platform["jammers"] = []
                        platform["comms"] = []
                    else:
                        platform["speed"] = float(d[INDEX_BLUE_TANK_SPEED])
                        platform["weapons"] = [{"weapon": "cannon", "domains": ["ground"], "load": "unlimited", "range": float(d[INDEX_BLUE_TANK_WEAPON_RANGE]), "PK": 1.0}] 
                        platform["sensors"] = [{"sensor": "EO/IR", "domains": ["ground"], "range": float(d[INDEX_BLUE_TANK_SENSOR_RANGE])}]
                        platform["jammers"] = []
                        platform["comms"] = []

                else:
                    # dont add anything else to plaforms
                    pass
                platforms[aid] = platform
                
            elif a["type"] in ["base","CAPLoc"]:
                location = {"type": a["type"], "allegiance": a["color"]}
                if "at" in a:
                    location["coordinates"] = [float(a["at"][0]), float(a["at"][1])]
                locations[aid] = location
            
    return {"units": units, "platforms": platforms, "locations": locations}

# compile data array for events for nominated agent id
# [eventid,time,significance,[elements],[parameters],description]
# simplifies some events by only recording state changes
def getEvents(data,eventsOfInterest=None,simplify=[]):
    def getEventSignificance(e):
        sig = eventSignificance['default']
        if 'predicate' in e:
            predicate = e['predicate']
            if predicate in eventSignificance:
                sig = eventSignificance[predicate]
        return sig

    #def getElementID(eid,etype,ecolor):
    #    return "%s(%s,%s)" % (etype,ecolor,eid)
        
    def simplifyEvent(event,eid,etime,epred,esubj,eobj):
        # make copy of event
        simple = copy.deepcopy(event)
        
        # update state and time and eid
        simple[0] = eid
        simple[1] = etime
        simple[4][0] = epred # predicate parameter
        simple[5] = "%s %s %s" % (esubj,epred,eobj) # description
        #print 'SIMPLIFIED EVENT: %s' % simple
        return simple
        
    eid = 0
    events = []
    
    # events in previous tick arranged in dict:
    # {<subject>: {<object>: {<predicate>: <event>}+]}+}
    prevEvents = None
    prevTracks = {}
    startedTracks = {}
    
    for d in data:
        currentEvents = {}
        time = tick2time(d[INDEX_TICK])
        
        evts = d[INDEX_EVENTS]
        locs = d[INDEX_LOCS]
        tracks = {}
        for a in locs:
            if ('at' in a) and (a['type'] not in NONTRACK_TYPES):
                #print "DEBUG:: coordinates: %s" % a
                aid = getEntityID(a['id'],a['type'],a['color'])
                tracks[aid] = a['at']

                # decide if we need to create a 'start' event - does this track exist in startedTracks?
                if not (aid in startedTracks):
                    startedTracks[aid] = {'time': time, 'at': tracks[aid]}
                    # set up event parameters
                    sig = 1.0
                    subj = aid
                    obj  = aid
                    predicate = 'start'
                    sloc = tracks[aid]
                    oloc = tracks[aid]
                    #event = [eid,time,sig,[subj, obj],[predicate,sloc,oloc],"%s %s %s" % (subj,predicate,obj)]
                    event = [eid,time,sig,[subj],[predicate,sloc,oloc],"%s %s %s" % (subj,predicate,obj)]
                    
                    # initialise events for this subject 
                    currentEvents[subj] = {obj: {predicate:event}}
                    
                    # increment event id
                    eid += 1                
                
        if len(evts) > 0:
            for e in evts:
                if 'predicate' in e:
                    predicate = e['predicate']
                    if eventsOfInterest is None or predicate in eventsOfInterest:
                        subj = getEntityID(e['subject'],e['subjectType'],e['subjectColor'])
                        obj  = getEntityID(e['object'],e['objectType'],e['objectColor'])
                        sig   = getEventSignificance(e)
                        sloc  = []
                        oloc  = []
                        if subj in tracks:
                            sloc = tracks[subj]
                        if obj in tracks:
                            oloc = tracks[obj]
                            
                        #event = [eid,time,sig,[subj, obj],[predicate],"%s %s %s" % (subj,predicate,obj)]
                        if subj == obj:
                            event = [eid,time,sig,[subj],[predicate,sloc,oloc],"%s %s %s" % (subj,predicate,obj)]
                        else:
                            event = [eid,time,sig,[subj, obj],[predicate,sloc,oloc],"%s %s %s" % (subj,predicate,obj)]                            
                        
                        if subj in currentEvents:
                            if obj in currentEvents[subj]:
                                currentEvents[subj][obj][predicate] = event
                            else:
                                # append to dict
                                currentEvents[subj][obj] = {predicate:event}
                        else:
                            # build new item in dict
                            currentEvents[subj] = {obj: {predicate:event}}

                        # increment event id
                        eid += 1
        
        # check for initialisation
        if not (prevEvents is None):        
            # see if any previousEvents no longer represented in currentEvents
            for psubj in prevEvents:
                for pobj in prevEvents[psubj]:
                    for ppred in prevEvents[psubj][pobj]:
                        event = copy.deepcopy(prevEvents[psubj][pobj][ppred])
                        if ppred in simplify:
                            # need to check if state changed in currentEvents
                            if (psubj in currentEvents) and (pobj in currentEvents[psubj]) and (ppred in currentEvents[psubj][pobj]):
                                # no change, be happy
                                pass
                            else:
                                #print "ENDING %s: %s %s %s" % (time,ppred,psubj,pobj)
                                # record event has ended if object is still in tracks
                                if (pobj in tracks) and (psubj in tracks):
                                    epred = 'ends.'+ppred
                                    events.append(simplifyEvent(event,eid,time,epred,psubj,pobj))
                                    #print "ADDED: %s" % events[-1]

                                    # update eid
                                    eid += 1
                        else:
                            # nothing to do with previous events if not simplifying
                            pass

        # now look if current events are new
        for csubj in currentEvents:
            for cobj in currentEvents[csubj]:
                for cpred in currentEvents[csubj][cobj]:
                    event = copy.deepcopy(currentEvents[csubj][cobj][cpred])
                    
                    # try to populate track with previous position if none available now
                    if event[4][1] == [] and csubj in prevTracks:
                        event[4][1] = prevTracks[csubj]
                    if event[4][2] == [] and cobj in prevTracks:
                        event[4][2] = prevTracks[cobj]

                    if cpred in simplify:
                        # need to check if this is a new event or not
                        # (structured like this to help debugging)
                        if prevEvents is None:
                            #print "INITIALISING %s: %s %s %s" % (time, cpred, csubj,cobj)
                            
                            # record new event
                            epred = 'begins.'+cpred
                            events.append(simplifyEvent(event,eid,time,epred,csubj,cobj))
                            #print "ADDED: %s" % events[-1]
                            eid += 1
                        elif (csubj in prevEvents) and (cobj in prevEvents[csubj]) and (cpred in prevEvents[csubj][cobj]):
                            # no change, be happy
                            pass
                        else:
                            # record new event
                            epred = 'begins.'+cpred
                            events.append(simplifyEvent(event,eid,time,epred,csubj,cobj))
                            #print "ADDED: %s" % events[-1]
                            
                            # update eid
                            eid += 1
                    else:
                        # if not simplifying, just add event
                        events.append(event)                
                        #print "ADDED: %s" % events[-1]
        
        # update previous events
        if prevEvents is None:
            #print "INITIALISING %s" % time
            if currentEvents != {}:
                #print "INITIALISED %s" % time
                # only change initialisation state if we have actually had an event
                prevEvents = currentEvents     
        else:
            prevEvents = currentEvents
        prevTracks = tracks
                
    return events

# return list with metrics at each time step
def getMetrics(data):
    metrics = []
    column_name_map = {
        "red_jets": INDEX_RED_JETS,
        "blue_jets": INDEX_BLUE_JETS,
        "red_gbads": INDEX_RED_GBADS, 
        "blue_gbads": INDEX_BLUE_GBADS,
        "red_tanks": INDEX_RED_TANKS,
        "blue_tanks": INDEX_BLUE_TANKS,
        "red_ddgs" : INDEX_RED_DDGS,
        "blue_ddgs" : INDEX_BLUE_DDGS,
        "reb_subs": INDEX_RED_SUBS ,
        "blue_subs": INDEX_BLUE_SUBS,
        "red_AIM9_fired": INDEX_RED_AIM9,
        "blue_AIM9_fired": INDEX_BLUE_AIM9,
        "red_AIM120_fired":INDEX_RED_AIM120,
        "blue_AIM120_fired": INDEX_BLUE_AIM120,
        "red_AGM88_fired": INDEX_RED_AGM88,
        "blue_AGM88_fired": INDEX_BLUE_AGM88,
    }
    labels = ["time"]
    labels.extend(list(column_name_map.keys()))
    reports = getReports(data,list(column_name_map.values()))
    
    for i,t in enumerate(reports[0]):
        data = {}
        for k,label in enumerate(labels):
            data[label] = reports[k][i]
        #print "%s: %s" % (i,data)
        metrics.append(data)
    return {"metrics": metrics}

# parse report
# returns list of dicts
def readreport(report):
    #scale netlogo back to map units
    def trans_x(x):
        map_xrange = MAP_BOUNDS[1]-MAP_BOUNDS[0]
        netlogo_xrange = PATCH_BOUNDS[1]-PATCH_BOUNDS[0]
        return (map_xrange*x+(MAP_BOUNDS[0]*PATCH_BOUNDS[1]-PATCH_BOUNDS[0]*MAP_BOUNDS[1]))/netlogo_xrange
    def trans_y(y):
        map_yrange = MAP_BOUNDS[3]-MAP_BOUNDS[2]
        netlogo_yrange = PATCH_BOUNDS[3]-PATCH_BOUNDS[2]
        return (map_yrange*y+(MAP_BOUNDS[2]*PATCH_BOUNDS[3]-PATCH_BOUNDS[2]*MAP_BOUNDS[3]))/netlogo_yrange

    result = ''
    
    if report[0] == '[':
        rs = report[1:-1].replace('[','').split(']')
        result = []
        for e in rs:
            if len(e) > 0:
                if e[0] == ' ':
                    # strip leading space
                    ee = e[1:]
                else:
                    ee = e
                    
                # parse data
                # split at spaces (now)
                es = ee.split()
                e2 = {}
                if len(es) > 0:
                    # handle different cases
                    # [<color>,<type>,<id>,'at',<xcor>,<ycor>]
                    if len(es) == 6 and es[3] == 'at':
                        e2 = {'at': [trans_x(float(es[4])),trans_y(float(es[5]))], 'color': es[0], 'type': es[1], 'id': int(es[2])}
                    # [<subjcolor>,<subjtype>,<subject>,<predicate>,<objcolor>,<objtype>,<object>]
                    elif len(es) == 7:
                        e2 = {'predicate': es[3], 'subject': es[2], 'subjectColor': es[0], 'subjectType': es[1], 'object': es[6], 'objectColor':es[4], 'objectType':es[5]}
                result.append(e2)
    else:
        result = report
    
    return result

def dobatch(run,data):
#     eventsFile = EVENTS_FILE.replace('.json',' - run %s.json' % run)
#     allEventsFile = ALL_EVENTS_FILE.replace('.json',' - run %s.json' % run)
#     tracksFile = TRACKS_FILE.replace('.json',' - run %s.json' % run)
#     platformsFile = PLATFORMS_FILE.replace('.json',' - run %s.json' % run)
#     metricsFile = METRICS_FILE.replace('.json',' - run %s.json' % run)
    eventsFile = EVENTS_FILE
    allEventsFile = ALL_EVENTS_FILE
    tracksFile = TRACKS_FILE
    platformsFile = PLATFORMS_FILE
    metricsFile = METRICS_FILE
    
    metrics = getMetrics(data)
    print ('Writing metrics to %s...' % (metricsFile))
    with open(metricsFile, 'w') as outfile:  
        json.dump(metrics, outfile)
        
    platforms = getPlatforms(data)
    print ('Writing platforms to %s...' % (platformsFile))
    with open(platformsFile, 'w') as outfile:  
        json.dump(platforms, outfile)    
    
    alltracks = getAllTracks(data)
    print ('Writing tracks to %s...' % (tracksFile))
    with open(tracksFile, 'w') as outfile:  
        json.dump(alltracks, outfile)
   
    events = getEvents(data,simplify=['tracking','jamming','linking','networking'])
    ejson = {'events':events}
    print ('Writing %s events to %s...' % (len(events),eventsFile))
    with open(eventsFile, 'w') as outfile:  
        json.dump(ejson, outfile)
    
    allevents = getEvents(data)
    ajson = {'events':allevents}
    print ('Writing %s events to %s...' % (len(allevents),allEventsFile)   )
    with open(allEventsFile, 'w') as outfile:  
        json.dump(ajson, outfile)
        
    print ("---")

# batch process data from jforce.
# not elegant but does the job.
# load file generated from jforce
def batchdata(filename,offset=7):  
    
    # read file
    rundata = []
    run = None
    print ("Reading data from %s..." % filename)
    with open(filename, 'r') as f:
        #lines = f.readlines()
        line = " "
        linenum = 0
        count = 0
        while line != "":
            line = f.readline()
            linenum += 1
            
            if (len(line) > 0) and (linenum > offset):
                newline = []
                # split list at ','
                listofline = line[1:].replace('"','').replace('\n','').split(',')
                
                # collect lines with selected run number
                #print "READ[%s]: %s" % (linenum,listofline)
                numrun = int(listofline[0])
                if run:
                    if numrun != run:
                        # process data collected for run so far
                        print ("%s lines processed for run %s" % (count, run))
                        
                        if len(rundata) > 0:
                            dobatch(run,rundata)
                        
                        run = numrun
                        count = 0
                        rundata = []
                        print ("Processing run %s" % run)
                else:
                    # set this as current run
                    run = numrun
                    count = 0
                    rundata = []
                    print ("Processing run %s" % run)
                
                # process data
                # need to parse each element
                for e in listofline:
                    newline.append(readreport(e))

                #    ee = e.replace('"','').replace('[','').split(']')
                #    line.append(ee)

                # append non-empty lines
                if len(newline) > 0:
                    rundata.append(newline)
                    count += 1


    #print lines
    #print "%s lines read from %s" % (len(lines),filename)
                        
        # process data collected for run so far
        print ("%s lines processed for run %s" % (count, run))
                        
        if len(rundata) > 0:
            dobatch(run,rundata)
            
        print ("fini!")

    return

for i in range( RANGE[0], RANGE[1]):
  # TODO update output file directory
  out_path = os.path.expanduser(OUTPUT_PATH + "/" + str(i) + "/" ) 
  os.makedirs(out_path, exist_ok=True)

  # define input and output file names
  # TODO update simulation raw file
  JFORCE_DATA_FILE = os.path.join(INPUT_PATH + "/output_" + str(i) + ".csv")

  # TODO update output file name
  ALL_EVENTS_FILE = out_path + "/jforce_v6a-allevents.json"
  EVENTS_FILE = out_path + "/jforce_v6a-events.json"
  TRACKS_FILE = out_path  + "/jforce_v6a-tracks.json"
  PLATFORMS_FILE = out_path  + "/jforce_v6a-scenario.json"
  METRICS_FILE = out_path  +  "/jforce_v6a-metrics.json"
  
  batchdata(JFORCE_DATA_FILE)
