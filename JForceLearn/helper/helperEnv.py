import os
import sys
from jsonc_parser.parser import JsoncParser


def get_raw_data_path():
    s = os.getenv('JFL_RAW_DATA_PATH')
    if s is None:
        sys.exit(
            "Raw data Path is undefined, please update `env.server` file and run `source env.server")
    return s


def get_config_path():
    s = os.getenv('JFL_CONFIG_PATH')
    if s is None:
        sys.exit(
            "Config Path is undefined, please update `env.server` file and run `source env.server")
    return s


SIM_CONFIG = None


def get_sim_config():
    global SIM_CONFIG
    if SIM_CONFIG is None:
        SIM_CONFIG = JsoncParser.parse_file(
            os.path.join(get_config_path(), "simulation/env.simulation.jsonc")
        )
    return SIM_CONFIG


ROOT_DIR = None


def set_root_dir(str):
    global ROOT_DIR
    ROOT_DIR = str


def get_root_dir():
    return ROOT_DIR
