import random
import joblib
import pandas as pd
import numpy as np
import JForceLearn.helper.helperEnv as helperEnv
from sklearn import metrics, preprocessing
from sklearn.compose import ColumnTransformer

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
import seaborn as sns

def seperate_normal_and_outlier_caseID(path_to_summary_csv, win_threshold, shuffle=False, reduce_sample=0, combine_outlier=False):
    final_jet_count = pd.read_csv(path_to_summary_csv)
    if reduce_sample > 0:
        final_jet_count = final_jet_count[:reduce_sample]
    # split the dataset using number of red_jet remaining
    if combine_outlier:
        tot = final_jet_count['trialID'].tolist()
        if shuffle:
            random.shuffle(tot)
        return tot, None
    else:
        outlier = final_jet_count[final_jet_count['red_jets']
                                  < win_threshold]['trialID'].tolist()
        normal = final_jet_count[final_jet_count['red_jets']
                                 >= win_threshold]['trialID'].tolist()
        # shuffle data if needed
        if shuffle:
            random.shuffle(normal)
            random.shuffle(outlier)
        return normal, outlier


def load_standard_scaler(file_name):
    return joblib.load(file_name)


def concate_dataset(list_of_case_id, data_path):
    combined = pd.DataFrame()
    for id in list_of_case_id:
        metrics_reduced = load_metric(id, data_path)
        combined = combined.append([metrics_reduced], ignore_index=True)
    return combined

def load_metric(id, data_path):
    return pd.read_csv(f"{data_path}/data/metric_{id}.csv")

# convert data into classificaion format 
def convert_to_classification_lstm_data(df, scaler, time_step, prediction_size, columns, overlap):
    ''' 
     0,1,2 -> dlt_red, dlt_blue\n
     1,2,3 -> dlt_red, dlt_blue
    '''
    red_index = df.columns.to_list().index("red_jets")
    blue_index = df.columns.to_list().index("blue_jets")
    # get wanted columns
    if columns is not None:
        df = df[columns]
    # apply scaling
    if scaler is not None:
        scaled_df = scaler.transform(df)
    else :
        scaled_df = df.values
    tmp_df = np.diff( np.insert(df[['red_jets', 'blue_jets']].values, [0], 0, axis=0) , axis=0)
    X = list()
    Y = list()
    for start in range(scaled_df.shape[0] - (time_step + prediction_size)):
        temp = scaled_df[start:start+time_step]
        temp2 = tmp_df[start+time_step-overlap:start+time_step+prediction_size-overlap]

        X.append(temp)
        Y.append(temp2)

    return X, Y, scaled_df


def convert_to_continuous_lstm_data(df, scaler, time_step, prediction_size, columns, overlap):
    ''' 
     0,1,2 -> 3, 4\n
     1,2,3 -> 4, 5
    '''
    # get wanted columns
    if columns is not None:
        df = df[columns]
    # apply scaling
    if scaler is not None:
        scaled_df = scaler.transform(df)
    else :
        scaled_df = df.values

    X = list()
    Y = list()
    for start in range(scaled_df.shape[0] - (time_step + prediction_size)):
        temp = scaled_df[start:start+time_step]
        temp2 = scaled_df[start+time_step-overlap:start+time_step+prediction_size-overlap]
        X.append(temp)
        Y.append(temp2)
    return X, Y, scaled_df


def convert_to_discrete_lstm_data(df, scaler, time_step, prediction_size, columns, overlap=0):
    '''
    0,1,2 -> 3, 4 \n
    3,4,5 -> 6, 7
    '''
    # get wanted columns
    if columns is not None:
        df = df[columns]
    # apply scaling
    if scaler is not None:
        scaled_df = scaler.transform(df)

    X = list()
    Y = list()
    for start in range(0, df.shape[0], prediction_size):
        temp = df[start: start + time_step]
        temp2 = df[start+time_step-overlap:start+time_step+prediction_size-overlap]
        if len(temp2) < prediction_size:
            continue
        X.append(temp)
        Y.append(temp2)
    return X, Y


def get_classification_data(list_of_case_id, data_path, scaler, time_step, prediction_size, columns, method="continuous", overlap=0):
    training_X = list()
    training_Y = list()
    one_hot = preprocessing.OneHotEncoder()
    one_hot.fit([[0],[-1],[-2], [-3], [-4], [-5], [-6], [-7], [-8], [-9], [-10]])
    for id in list_of_case_id:
        metrics_reduced = load_metric(id, data_path)

        x, y, _ = convert_to_classification_lstm_data(metrics_reduced, scaler, time_step, prediction_size, columns, overlap) if (
            method == "continuous") else convert_to_continuous_lstm_data(metrics_reduced, scaler, time_step, prediction_size, columns, overlap)

        training_X.extend(x)
        training_Y.extend(y)
    
    y = np.stack(training_Y)
    return np.stack(training_X), [one_hot.transform(y[:,:,0]).toarray(),one_hot.transform(y[:,:,1]).toarray() ], one_hot


def get_lstm_data(list_of_case_id, data_path, scaler, time_step, prediction_size, columns, method="continuous", overlap=0):
    training_X = list()
    training_Y = list()
    for id in list_of_case_id:
        metrics_reduced = load_metric(id, data_path)

        x, y, _ = convert_to_continuous_lstm_data(metrics_reduced, scaler, time_step, prediction_size, columns, overlap) if (
            method == "continuous") else convert_to_continuous_lstm_data(metrics_reduced, scaler, time_step, prediction_size, columns, overlap)

        training_X.extend(x)
        training_Y.extend(y)
    
    return np.stack(training_X), np.stack(training_Y)

def split_xy_set(x, y, test_size=0.1):
    return x[:int(len(x)*(1-test_size))], x[int(len(x)*(1-test_size)):], y[:int(len(y)*(1-test_size))], y[int(len(y)*(1-test_size)):]

def split_xy_3set(x, y, test_size=0.1, preserved=0.1):
    return x[:int(len(x)*(1-test_size-preserved))], x[int(len(x)*(1-test_size-preserved)):int(len(x)*(1-preserved))], x[int(len(x)*(1-preserved)):], \
     y[:int(len(y)*(1-test_size-preserved))], y[int(len(y)*(1-test_size-preserved)):int(len(y)*(1-preserved))],y[int(len(y)*(1-preserved)):]

def split_x(x, test_size=0.1, preserved=0.1):
    return x[:int(len(x)*(1-test_size-preserved))], x[int(len(x)*(1-test_size-preserved)):int(len(x)*(1-preserved))], x[int(len(x)*(1-preserved)):]

# write time log to file
def create_timeing_txt(timing_list, partition_list):
    if len(timing_list) < 2:
        return [],
    l = []
    for i in range(len(timing_list)-1):
        l = np.append(l, f"{partition_list[i]}: {timing_list[i] - timing_list[i+1]}")
    l = np.append(l, f"Total Time Spent: f{timing_list[0] - timing_list[-1]}")
    return l

# group bleu and red data into one axis (separate feature)
def joint_view(predictions, full_y_true, time_arr, time_step, time_eclapse, error_obj = None, title="my plot", save_file_name=None):
    markers= ["+", "x"]
    colors = ["red", "blue"]
    t_colors = ["crimson", "darkblue"]
    # #create a drawing board
    fig, ax = plt.subplots(4, 1, figsize=(15,8))
    r_index = 0 
    c_index = 0

    cols = range(1, 5) if "time" in full_y_true.columns else range(0,4)
    
    for col in cols:
        if r_index >3:
            r_index = 0
            c_index = 1
        
        cur_ax = ax[cols.index(col)]

        # #plot true
        for c in t_colors:
            curr_col = col if c == 'crimson' else col + 4
            cur_ax.plot(time_arr, full_y_true[full_y_true.columns[curr_col]], color=c)

        # #plot prediction
        # for step in range(predictions.shape[0]):
        lines = []
        labels = []
        # for step in [0, 4]:
        for step in [0]:
            for c in colors:
                
                curr_col = col if c == 'red' else col + 4
                ivt_step = time_step - step - 1
                # plot every step in a chart
                to_be_plot = predictions[step][:, curr_col]
                min_len = min(len(time_arr) - step, len(to_be_plot))
                lines.append(
                    cur_ax.scatter(time_arr[-min_len-ivt_step:len(time_arr)-ivt_step], to_be_plot[:min_len], marker=markers[step], color=c, norm=step/10, alpha=0.4)
                )
                labels.append( f"{c} - window {step+1}" )

        # plot divergence rate 
        if error_obj is not None:
            secax_y = cur_ax.twinx() 
            # secax_y_2 = cur_ax.twinx() 
            # mse
            if error_obj["mse_arr"] is not None:
                mse_arr = error_obj["mse_arr"]
                min_len = min(len(time_arr) - time_step, len(mse_arr))
                secax_y.bar(time_arr[-min_len-time_step+1:-time_step+1], mse_arr, color='DarkSlateGray',alpha=0.3)
            
            # # mspe
            # if error_obj["mspe_arr"] is not None:
            #     mspe_arr = error_obj["mspe_arr"]
            #     min_len = min(len(time_arr) - time_step, len(mspe_arr))
            #     secax_y.bar(time_arr[-min_len-time_step+1:-time_step+1], mspe_arr, color='yellow',alpha=0.3)

            # mse score
            if error_obj["mse_score"] is not None:
                mse_score = error_obj["mse_score"]
                min_len = min(len(time_arr) - time_step, len(mse_score))
                secax_y.plot(time_arr[-min_len-time_step+1:-time_step+1], np.abs(mse_score), color='DarkSlateGray',alpha=0.7)
            
            # #mspe score
            # if error_obj["mspe_score"] is not None:
            #     mspe_score = error_obj["mspe_score"]
            #     min_len = min(len(time_arr) - time_step, len(mspe_score))
            #     secax_y.plot(time_arr[-min_len-time_step+1:-time_step+1], mspe_score, color='yellow',alpha=0.7)


    # error_obj["mse_score"]
    # error_obj["mspe_arr"]
    # error_obj["mspe_score"]
        cur_ax.set_title(full_y_true.columns[col])
        cur_ax.set_ylim(ymin=-1)
        cur_ax.set_xlim(xmin=0)
        cur_ax.legend(lines, labels)
        r_index += 1

    plt.subplots_adjust(hspace=0.5)
    if save_file_name is None:
        fig.suptitle(title)
    else:
        plt.savefig(f"{save_file_name}")
    plt.cla()
    plt.clf()
    plt.close('all')
    plt.show()
    print("end")
    return

# plot everything in own axis
def split_view(predictions, full_y_true, time_arr, time_step, time_eclapse, error_obj = None, title="my plot", save_file_name=None):
    markers= ["1","2","3", "4", "+", "x"]
    # #create a drawing board
    fig, ax = plt.subplots(2, 1, figsize=(15,8))
    # fig, ax = plt.subplots(4, 2, figsize=(15,8))
    r_index = 0 
    c_index = 0

    cols = [1, 5] if "time" in full_y_true.columns else [0, 4]
    
    for col in cols:
    # for col in range(predictions.shape[2]): # [ step, time, columns ]
    # if (full_y_true.columns[col] == "time"):
    #     print("skip time column")
    #     # continue
        if r_index >3:
            r_index = 0
            c_index = 1
        
        cur_ax = ax[cols.index(col)]

        # #plot true
        cur_ax.plot(time_arr, full_y_true[full_y_true.columns[col]], color="black")

        # #plot prediction
        # for step in range(predictions.shape[0]):
        lines = []
        labels = []
        # for step in [0, 4]:
        for step in [0]:
            ivt_step = time_step - step - 1
            # plot every step in a chart
            to_be_plot = predictions[step][:, col]
            min_len = min(len(time_arr) - step, len(to_be_plot))
            lines.append(
                cur_ax.scatter(time_arr[-min_len-ivt_step:len(time_arr)-ivt_step], to_be_plot[:min_len], marker=markers[step],cmap="Set1", norm=step/10)
            )
            labels.append( f"window {step+1}" )
        # plot divergence rate 
        if error_obj is not None:
            secax_y = cur_ax.twinx() 
            # secax_y_2 = cur_ax.twinx() 
            # mse
            if error_obj["mse_arr"] is not None:
                mse_arr = error_obj["mse_arr"]
                min_len = min(len(time_arr) - time_step, len(mse_arr))
                secax_y.bar(time_arr[-min_len-time_step+1:-time_step+1], mse_arr, color='DarkSlateGray',alpha=0.3)
            
            # # mspe
            # if error_obj["mspe_arr"] is not None:
            #     mspe_arr = error_obj["mspe_arr"]
            #     min_len = min(len(time_arr) - time_step, len(mspe_arr))
            #     secax_y.bar(time_arr[-min_len-time_step+1:-time_step+1], mspe_arr, color='yellow',alpha=0.3)

            # mse score
            if error_obj["mse_score"] is not None:
                mse_score = error_obj["mse_score"]
                min_len = min(len(time_arr) - time_step, len(mse_score))
                secax_y.plot(time_arr[-min_len-time_step+1:-time_step+1], np.abs(mse_score), color='DarkSlateGray',alpha=0.7)
            
            # #mspe score
            # if error_obj["mspe_score"] is not None:
            #     mspe_score = error_obj["mspe_score"]
            #     min_len = min(len(time_arr) - time_step, len(mspe_score))
            #     secax_y.plot(time_arr[-min_len-time_step+1:-time_step+1], mspe_score, color='yellow',alpha=0.7)


    # error_obj["mse_score"]
    # error_obj["mspe_arr"]
    # error_obj["mspe_score"]
        cur_ax.set_title(full_y_true.columns[col])
        cur_ax.set_ylim(ymin=-1)
        cur_ax.set_xlim(xmin=0)
        cur_ax.legend(lines, labels)
        r_index += 1

    plt.subplots_adjust(hspace=0.5)
    if save_file_name is None:
        fig.suptitle(title)
    else:
        plt.savefig(f"{save_file_name}")
    plt.cla()
    plt.clf()
    plt.close('all')
    plt.show()
    print("end")
    return

def mean_sequare_positive_error(pred_1Darr, truth_1Darr):
    if len(pred_1Darr) != len(truth_1Darr):
        return -1
    else:
        return np.mean(np.square(np.maximum(0, pred_1Darr - truth_1Darr)))
def mean_sequare_negative_error(pred_1Darr, truth_1Darr):
    if len(pred_1Darr) != len(truth_1Darr):
        return -1
    else:
        return np.mean(np.square(np.minimum(0, pred_1Darr - truth_1Darr)))

# use prediction result in next prediction
def predict_long_time_forecast(model, df, scaler, time_step, prediction_size, columns, is_continuous = True, round_pred_to = 0, use_future=1, overlap=0):
    SIM_CONFIG = helperEnv.get_sim_config()
    features = len(columns)
    use_future = min(use_future, prediction_size)
    use_future = max(use_future, 1)
    time_index = columns.index("time") if "time" in columns else None
    red_index = columns.index("red_jets")
    blue_index = columns.index("blue_jets")
    test_x, test_y, scaled_df= convert_to_continuous_lstm_data(df, scaler=scaler, time_step=time_step, prediction_size=prediction_size, columns=columns, overlap=overlap) if is_continuous else convert_to_discrete_lstm_data(df, scaler=scaler, time_step=time_step, prediction_size=prediction_size, columns=columns, overlap=overlap)

    predictions = [ None for x in range(prediction_size)] # pd.DataFrame(columns=COLUMNS)
    history = test_x[0]
    for j in range(0, len(test_x)):
        x = history[-time_step:]
        next_tick = model.predict(x.reshape(1, x.shape[0], x.shape[1]), verbose=0)
        pred_scaled = next_tick.reshape(next_tick.shape[1], next_tick.shape[2]) if len(next_tick.shape) > 2 else next_tick
        pred = np.round(scaler.inverse_transform(next_tick.reshape(next_tick.shape[1], next_tick.shape[2]) if len(next_tick.shape) >2 else next_tick  ), round_pred_to)
        for i in range(len(pred)):
            if predictions[i] is None:
                predictions[i] = pred[i]
            else:
                predictions[i] = np.vstack((predictions[i], pred[i]))
        to_be_append = pred_scaled[0:use_future]
        tmp = scaled_df[len(history)-1: len(history)-1 + use_future]

        # replace acutal jets number by predicted jet number
        for c in range(use_future):
            np.put(tmp[c], [red_index, blue_index], to_be_append[c][[red_index, blue_index]])
        if time_index is not None:
            for c in range(use_future):
                np.put(to_be_append[c], [time_index], test_y[j][time_index])
        history = np.append(history, tmp, axis=0)
    result = np.array(predictions)
    return result

# normal prediction 
def predict_simulation(model, df, scaler, time_step, prediction_size,
  columns, overlap=0, divergence_columns=None, is_continuous = True, round_pred_to = 0, calc_divg = False):
    SIM_CONFIG = helperEnv.get_sim_config()
    features = len(columns)
    if divergence_columns is not None:
        mse = [ columns.index(x) for x in divergence_columns]

    # time_intervel = SIM_CONFIG["LOG_INTERVAL"] * time_eclapse if time_eclapse > 0 else SIM_CONFIG["LOG_INTERVAL"]
    
    # get lstm format data
    test_x, test_y, _ = convert_to_continuous_lstm_data(df, scaler=scaler, time_step=time_step, prediction_size=prediction_size, columns=columns, overlap=overlap) if is_continuous else convert_to_discrete_lstm_data(df, scaler=scaler, time_step=time_step, prediction_size=prediction_size, columns=columns, overlap=overlap)

    predictions_scaled = list()

    # grouping into t(x+1), t(x+2), ... , t(x+n) array
    group_view_predictions = [ None for x in range(prediction_size)] # pd.DataFrame(columns=COLUMNS)
    
    mspe_arr_window = []
    mse_arr_window = []
    mse_w_score = []
    mspe_w_score= []

    for j in range(0, len(test_x)):
        x =  test_x[j]
        next_tick = model.predict(x.reshape(1, x.shape[0], x.shape[1]), verbose=0)
        pred_scaled = next_tick.reshape(next_tick.shape[1], next_tick.shape[2])
        pred = np.round(scaler.inverse_transform(next_tick.reshape(next_tick.shape[1], next_tick.shape[2])), round_pred_to) if scaler is not None else pred_scaled
        # pred = scaler.inverse_transform(next_tick.reshape(next_tick.shape[1], next_tick.shape[2])) if scaler is not None else pred_scaled
        for i in range(len(pred)):
            if group_view_predictions[i] is None:
                group_view_predictions[i] = pred[i]
            else:
                group_view_predictions[i] = np.vstack((group_view_predictions[i], pred[i]))
        if calc_divg:
            truth = scaler.inverse_transform(test_y[j]) if scaler is not None else test_y[j]
            mse_arr_window = np.append(mse_arr_window, metrics.mean_squared_error(truth[:, mse], pred[:, mse], squared=False))
            mspe_arr_window = np.append(mspe_arr_window, mean_sequare_positive_error(pred[:, mse], truth[:, mse]))
    
    # calculate divergence score 
    if calc_divg:
        mspe_w_score = np.diff(np.insert(mspe_arr_window, 0, 0))
        mse_w_score = np.diff(np.insert(mse_arr_window, 0, mse_arr_window[0]))

    # convert to dataFrame
    # for i in range(0, len(predictions)):
    #     predictions[i] = pd.DataFrame(data=predictions[i], columns=columns)
    result = np.array(group_view_predictions)
    return result, mse_arr_window, mse_w_score, mspe_arr_window, mspe_w_score

# predict t, and t+1 with t result unit t + next_n_time
def mixed_forecast(model, df, scaler, time_step, prediction_size, columns, is_continuous = True, round_pred_to = 0, use_future=1, overlap=0, next_n_time=5):
    SIM_CONFIG = helperEnv.get_sim_config()
    features = len(columns)
    use_future = min(use_future, prediction_size)
    use_future = max(use_future, 1)
    time_index = columns.index("time") if "time" in columns else None
    red_index = columns.index("red_jets")
    blue_index = columns.index("blue_jets")
    mse_arr = []
    mse_arr_jet = []
    l1e_arr = []
    l1e_arr_jet = []

    predictions = [  ]

    test_x, test_y, scaled_df= convert_to_continuous_lstm_data(df, scaler=scaler, time_step=time_step, prediction_size=prediction_size, columns=columns, overlap=overlap) if is_continuous else convert_to_discrete_lstm_data(df, scaler=scaler, time_step=time_step, prediction_size=prediction_size, columns=columns, overlap=overlap)
    for i in range(0, len(test_x)):
        history = test_x[i]
        preiction_scaled_list = []
        history_y = test_y[i: i+ next_n_time]

        for p in range(0, next_n_time):
            x = history[-time_step:]
            next_tick = model.predict(x.reshape(1, x.shape[0], x.shape[1]), verbose=0)
            pred_scaled = next_tick.reshape(next_tick.shape[1], next_tick.shape[2]) if len(next_tick.shape) > 2 else next_tick
            pred = np.round(scaler.inverse_transform(next_tick.reshape(next_tick.shape[1], next_tick.shape[2]) if len(next_tick.shape) >2 else next_tick  ), round_pred_to)
            
            if p==0 :
                predictions.append(pred[0])
            
            preiction_scaled_list.append(pred_scaled)

        # calculate mse
        mse = []
        l1e = []
        mse_jets = []
        l1e_jets = []
        for k in range(len(history_y)):
            mse.append( np.mean(np.square(preiction_scaled_list[k].reshape(-1) - history_y[k].reshape(-1) )) )
            mse_jets.append( np.mean(np.square(preiction_scaled_list[k].reshape(-1)[[0,4]] - history_y[k].reshape(-1)[[0,4]] )) )
            l1e.append( np.mean(preiction_scaled_list[k].reshape(-1) - history_y[k].reshape(-1) ) )
            l1e_jets.append( np.mean(preiction_scaled_list[k].reshape(-1)[[0,4]] - history_y[k].reshape(-1)[[0,4]] ) )
            # mse.append( np.mean(np.square(preiction_scaled_list[k].reshape(-1)[[red_index, blue_index]] - history_y[k].reshape(-1)[[red_index, blue_index]] )) )
        mse_arr.append(sum(mse))
        mse_arr_jet.append(sum(mse_jets))
        l1e_arr.append(sum(l1e))
        l1e_arr_jet.append(sum(l1e_jets))
    
    return np.array(predictions), np.array(mse_arr), np.array(mse_arr_jet), np.array(l1e_arr), np.array(l1e_arr_jet)

# use couple with mixed_forecast, plot line with divergence in color strength
def mix_view(predictions, full_y_true, time_arr, time_step, time_eclapse, start_prediction=0, mse = None, title="my plot", id=None, outpath=None):
    markers= ["+", "x"]
    colors = ["red", "blue"]
    t_colors = ["crimson", "darkblue"]
    # #create a drawing board
    
    r_index = 0 
    #c_index = 0

    red_cols = range(1, 5) if "time" in full_y_true.columns else range(0,4)
    blue_cols = [x + 4 for x in red_cols]
    mse = mse / np.mean(mse)
    norm = mpl.colors.Normalize(vmin=0, vmax=1)
    #plot red
    fig, ax = plt.subplots(4, 1, figsize=(15,8))
    for col in red_cols:
        cur_ax = ax[red_cols.index(col)]
        cur_val = predictions[:, col]
        time = time_arr[start_prediction+time_step:start_prediction+time_step+cur_val.shape[0]]
        
        tmp = list(zip(time,cur_val))
        points = np.array(tmp).reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        # plot truth
        cur_ax.plot(time_arr, full_y_true[full_y_true.columns[col]], color="black", alpha=0.2)
        
        # plot pred
        lc = LineCollection(segments, cmap='Reds', norm=norm)
        lc.set_array(mse)
        line = cur_ax.add_collection(lc)
        fig.colorbar(line, ax=cur_ax, pad=0.01)

        cur_ax.set_title(full_y_true.columns[col])
        cur_ax.set_ylim(ymin=0)
        cur_ax.set_xlim(xmin=0)

    plt.subplots_adjust(top=0.9, left=0.05, bottom=0.05, right=1, hspace=0.5)

    fig.suptitle(f"red status id: {id}")
    plt.savefig(f"{outpath}/plt_red_{id}.png")
    plt.cla()
    plt.clf()
    plt.close('all')

    #plot blue
    fig, ax = plt.subplots(4, 1, figsize=(15,8))
    for col in blue_cols:
        cur_ax = ax[blue_cols.index(col)-4]
        cur_val = predictions[:, col]
        time = time_arr[start_prediction+time_step:start_prediction+time_step+cur_val.shape[0]]
        
        tmp = list(zip(time,cur_val))
        points = np.array(tmp).reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        # plot truth
        cur_ax.plot(time_arr, full_y_true[full_y_true.columns[col]], color="black", alpha=0.2)
        
        # plot pred
        lc = LineCollection(segments, cmap='Blues', norm=norm)
        lc.set_array(mse)
        line = cur_ax.add_collection(lc)
        fig.colorbar(line, ax=cur_ax, pad=0.01)

        cur_ax.set_title(full_y_true.columns[col])
        cur_ax.set_ylim(ymin=0)
        cur_ax.set_xlim(xmin=0)

    plt.subplots_adjust(top=0.9, left=0.05, bottom=0.05, right=0.99, hspace=0.5, wspace=0.01)

    fig.suptitle(f"blue status id: {id}")
    plt.savefig(f"{outpath}/plt_blue_{id}.png")
    plt.cla()
    plt.clf()
    plt.close('all')

    return

    # actual_arr = STANDARD_SCALER.inverse_transform(test_y[:, 0][:].reshape(test_y.shape[0], test_y.shape[2])) if not stateful \
    #     else STANDARD_SCALER.inverse_transform(test_y[:].reshape(test_y.shape[0] * test_y.shape[1], test_y.shape[2]))
    
    # # actual_arr = orig_y[:, 0][:] # get all first row of the orig_y value

    # columns= COLUMNS
    # pred_columns = PRED_COLUMNS
    # if DROP_TIME:
    #     columns = columns[1:]
    #     pred_columns = pred_columns[1:]
    # actual_df = pd.DataFrame(actual_arr, columns=columns)
    
    # predictions_data_set_format = list()
    # pred_arr = None

    # pred_scaled_df = None
    # pred_df = None
    # summary = {
    #     "_mean": list(),
    #     "_max": list(),
    #     "_min": list()
    # }

    # mse = list()
    # if not stateful:
    #     print(f"Stateless-use prediction:{USE_PREDICT}")
    #     if not use_prediction:
    #         for i in range(0, test_x.shape[0]):
    #             x =  test_x[i]
    #             next_tick = predict_next(model, x.reshape(1, x.shape[0], x.shape[1]))
    #             predictions_data_set_format.append(next_tick.reshape(test_y.shape[1], test_y.shape[2]))

    #             # # pick the most reasonable prediction with out_size options
    #             # pred = STANDARD_SCALER.inverse_transform(next_tick[0][0].reshape(1, next_tick[0][0].shape[0]))
    #             # np.round(pred, round_pred_to)
    #             # print(pred[0][0]) # print time
    #             # # mse.append([i+time_step, cal_diversion_msg(test_y[i][0],next_tick[0][0])])
    #             # mse.append([i+time_step, cal_diversion_msg(actual_arr[i],pred[0])])
    #             # predictions_scaled.append(next_tick[0][0])
    #             # predictions.append(pred[0])
    #     else :
    #         history = test_x[0]
    #         for i in range(0, test_x.shape[0]):
    #             x = history[i:i+time_step]
    #             # x = history[-time_step:]
    #             next_tick = predict_next(model, x.reshape(1, x.shape[0], x.shape[1]))
    #             predictions_data_set_format.append(next_tick.reshape(test_y.shape[1], test_y.shape[2]))
    #         # replacing next tick value with predicted value
    #             for ele in range(0, next_tick.shape[1]):
    #                 row = next_tick[0][ele]
    #                 row_y = test_y[i][ele]

    #                 # replace time prediction if time exist
    #                 if not DROP_TIME:
    #                     np.put(row, [0], row_y[0])

    #                 # if multi step prediction use the better prediction
    #                 if i+time_step+ele < len(history):
    #                     current_his = history[i+time_step+ele]
    #                     if mean_squared_error( row_y, current_his) > mean_squared_error( row_y, row):
    #                         history[i+time_step+ele] = row
    #                 else:
    #                     history = np.vstack( (history, row) )

    # else:
    #     print(f"Stateful-use prediction:{USE_PREDICT}")
    #     if not use_prediction:
    #         for i in range(0, test_x.shape[0]):
    #             x =  test_x[i]
    #             next_tick = predict_next(model, x.reshape(1, x.shape[0], x.shape[1]))
    #             predictions_data_set_format.append(next_tick.reshape(test_y.shape[1], test_y.shape[2]))
    #     else :
    #         history = test_x[0]
    #         for i in range(0, test_x.shape[0]):
    #             x = history[-time_step:]
    #             next_tick = predict_next(model, x.reshape(1, x.shape[0], x.shape[1]))
    #             predictions_data_set_format.append(next_tick.reshape(test_y.shape[1], test_y.shape[2]))
    #         # replacing next tick value with predicted value
    #             for ele in range(0, next_tick.shape[1]):
    #                 row = next_tick[0][ele]
    #                 row_y = test_y[i][ele]

    #                 # replace time prediction if time exist
    #                 if not DROP_TIME:
    #                     np.put(row, [0], row_y[0])

    #                 # if multi step prediction use the better prediction
    #                 if i+time_step+ele+out_size < len(history):
    #                     current_his = history[i+time_step+ele]
                        
    #                     if mean_squared_error( row_y, current_his) > mean_squared_error( row_y, row):
    #                         history[i+time_step+ele] = row
    #                 else:
    #                     history = np.vstack( (history, row) )

    # if not stateful:
    #     predictions = order_by_time(predictions_data_set_format, out_size)

    #     if all([ len(i) == 1 for i in predictions]):
    #         pred_arr = np.stack([arr[0] for arr in predictions])
    #         pred_scaled_df = pd.DataFrame(pred_arr, columns=pred_columns)
    #         summary["_mean"] = pred_arr
    #         summary["_max"] = pred_arr
    #         summary["_min"] = pred_arr
    #     else :
    #         temp_arr =list()
    #         for arr in predictions:
    #             temp_arr.append(arr[0])
    #             _df = pd.DataFrame(arr)
    #             summary["_mean"].append(_df.mean())
    #             summary["_max"].append(_df.max())
    #             summary["_min"].append(_df.min())
    #         pred_scaled_df = pd.DataFrame(temp_arr, columns=pred_columns)
    # else:
    #     prediction = np.concatenate(predictions_data_set_format).ravel().reshape(-1,features)
    #     pred_scaled_df = pd.DataFrame(prediction, columns=pred_columns)
    #     summary["_mean"] = prediction
    #     summary["_max"] = prediction
    #     summary["_min"] = prediction

    # pred_df = STANDARD_SCALER.inverse_transform(pred_scaled_df)
    # pred_df = pd.DataFrame(pred_df, columns=pred_columns)
    # summary["_mean"] = STANDARD_SCALER.inverse_transform(summary["_mean"])
    # summary["_max"] = STANDARD_SCALER.inverse_transform(summary["_max"])
    # summary["_min"] = STANDARD_SCALER.inverse_transform(summary["_min"])

    # if round_pred_to >-1:
    #     pred_df = pred_df.round(round_pred_to)
    #     summary["_mean"] = summary["_mean"].round(round_pred_to)
    #     summary["_max"] = summary["_max"].round(round_pred_to)
    #     summary["_min"] = summary["_min"].round(round_pred_to)

    # time = starting_index if not stateful else starting_index * out_size
    # for r in range(len(actual_df)):
    #     if not drop_time:
    #         time = actual_df['time'].iloc[r]

    #     mse.append([time, cal_divergence_mse(actual_df.iloc[r], pred_df.iloc[r])])

    #     if drop_time:
    #         time += 1
    
    # mse = pd.DataFrame(mse, columns=['time', 'mse'])
    # mse_reduced = mse[:]
    # # sorting means srquare error descending
    # # mse_sorted_df = mse_reduced.sort_values(by=['mse'], ascending=False)
    # # mse_sorted_df = mse_sorted_df.reset_index()
    # if 'time' not in actual_df.columns:
    #     actual_df = pd.DataFrame(mse['time']).join(actual_df)
    #     pred_df = pd.DataFrame(mse['time'].values, columns=["pred_time"]).join(pred_df)

    # return pred_df, actual_df, mse, summary