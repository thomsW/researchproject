import os
from datetime import datetime
import pickle
import importlib
import pathlib
from jsonc_parser.parser import JsoncParser
import numpy as np

from tensorflow.keras.models import load_model
import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau

from JForceLearn.helper.helperEnv import get_raw_data_path, get_sim_config, get_root_dir
import JForceLearn.helper.helper as helper
from tensorflow.keras.callbacks import CSVLogger

MODEL_CHECKPOINT = ""
VERBOSE = 1

# SETTING UP CHECKPOINTS, CALLBACKS AND REDUCING LEARNING RATE


def callback_function_parameter(out_path):
    # simple early stopping
    es_single = tf.keras.callbacks.EarlyStopping(
        monitor='val_loss', mode='min', verbose=VERBOSE, patience=10)
    # Save the best weights of the model using ModelCheckpoint
    mc_single = ModelCheckpoint(
        MODEL_CHECKPOINT, monitor='val_loss', mode='min', verbose=VERBOSE, save_best_only=True)
    lr_single = ReduceLROnPlateau(
        monitor='val_loss', patience=5, verbose=VERBOSE, factor=0.4, min_lr=0.00001)
    csv_logger = CSVLogger(f'{out_path}/training.log', separator=',', append=False)
    return [es_single, lr_single, mc_single, csv_logger]


def main(config, verbose=None):
    global VERBOSE, MODEL_CHECKPOINT
    if verbose is not None:
        VERBOSE = verbose

    # load create model function
    modelFn = importlib.import_module(config["M_Version"])

    # get parameters out from config json
    out_dir = config["OUT_DIR"]
    time_step = config["TIMESTEPS"]
    prediction_size = config["PREDICT_SIZE"]
    columns = config["COLUMNS"]
    features = len(columns)
    win_threshold = config["WIN_THRESHOLD"]
    mse_labels = config["MSE_LABELS"]
    epochs = config["EPOCHS"]
    batch_size = config["BATCH_SIZE"]
    overlap = config['OVERLAP'] if 'OVERLAP' in config else 0
    is_include_outlier = config["INCLUDE_OUTLIER"]
    data_path = os.path.join(
        get_root_dir(), f"JForceLearn/preprocessing/{config['D_Version']}")

    # creaete output folder
    out_path = os.path.join(
        get_root_dir(), f"JForceLearn/result/{config['OUT_DIR']}")
    model_out = f"{out_path}/out.model"
    MODEL_CHECKPOINT = f"{out_path}/{config['CHECKPOINT_NAME']}"
    pathlib.Path(out_path).mkdir(parents=True, exist_ok=True)
    pathlib.Path(f"{out_path}/prediction/truth/data/").mkdir(parents=True, exist_ok=True)
    pathlib.Path(f"{out_path}/prediction/estimate/data/").mkdir(parents=True, exist_ok=True)
    print(f"\n{data_path}\n")

    time_1 = datetime.now()

    normal_ids, outlier_ids = helper.seperate_normal_and_outlier_caseID( reduce_sample=5000,
        path_to_summary_csv=f"{data_path}/summary.csv", win_threshold=win_threshold, combine_outlier=is_include_outlier)
    training_set, validate_set, test_set = helper.split_x(normal_ids)
    # load standard scaler
    # scaler = helper.load_standard_scaler(f"{data_path}/jet_only_standard_scaler.bin") if "time" in columns else helper.load_standard_scaler(f"{data_path}/jet_only_standard_scaler_no_time.bin")
    scaler = helper.load_standard_scaler(f"{data_path}/standard_scaler.bin") if "time" in columns else helper.load_standard_scaler(f"{data_path}/standard_scaler_no_time.bin")
    
    # get training data
    training_x, training_y, one_hot_encoder = helper.get_classification_data(
        training_set, data_path=data_path, scaler=scaler, time_step=time_step, prediction_size=prediction_size, columns=columns, overlap=overlap)
    valid_x, valid_y, _ = helper.get_classification_data(
        validate_set, data_path=data_path, scaler=scaler, time_step=time_step, prediction_size=prediction_size, columns=columns, overlap=overlap)

    time_2 = datetime.now()

    # with open('lstm_data_format/10to5/server_5000_timeless_training_X.npy', 'wb') as f:
    #     np.save(f, training_x)
    # with open('lstm_data_format/10to5/server_5000_timeless_training_Y.npy', 'wb') as f:
    #     np.save(f, training_y)

    # with open('lstm_data_format/10to5/server_5000_timeless_valid_X.npy', 'wb') as f:
    #     np.save(f, valid_x)
    # with open('lstm_data_format/10to5/server_5000_timeless_valid_Y.npy', 'wb') as f:
    #     np.save(f, valid_y)

    # create model
    model = modelFn.create_model(time_step, len(columns), len(one_hot_encoder.categories_[0]))

    # vad_x, vad_y = convert_lstm_data_set(df=sampling_dataset(212))

    
    model.fit(training_x, training_y, epochs=epochs, batch_size=batch_size, validation_data=(
        valid_x, valid_y), verbose=VERBOSE, callbacks=callback_function_parameter(out_path=out_path))

    time_3 = datetime.now()

    # log down timing
    timing = helper.create_timeing_txt([time_1, time_2, time_3], [
                                       "convert Dataset Time", "Training Time"])
    f = open(f"{out_path}/timing_0.txt", "w+")
    for s in timing:
        f.write(f"{s}\n")
    f.close()

    # save model
    model.save(f"{model_out}.tf", save_format="tf")

    print("classification training model - end")
    return None
