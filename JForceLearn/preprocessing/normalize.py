
import numpy as np
import pandas as pd
import json
import os
import shutil
import joblib
import pathlib

from sklearn.preprocessing import StandardScaler, MaxAbsScaler, MinMaxScaler, RobustScaler
from JForceLearn.helper.helperEnv import get_raw_data_path, get_sim_config, get_root_dir

RAW_DATA_PATH = get_raw_data_path()
DATASET_PATH = os.path.join(get_root_dir(), "JForceLearn/preprocessing")
OUT_PATH = None


def sampling_dataset(id, path, sampled_len, sample_interval, fill_space, scale_down_time, normalize_columns, save_metric=True):
    FILE_NAME = f'{path}/jforce_v6a-metrics.json'
    f = open(FILE_NAME)
    metrics = json.load(f)
    # closing file
    f.close()
    metrics_df = pd.json_normalize(metrics['metrics'])
    metrics_cp = metrics_df[['time', 'red_jets', 'red_AIM9_fired', 'red_AGM88_fired', 'red_AIM120_fired',
                             'blue_jets', 'blue_AIM9_fired', 'blue_AGM88_fired', 'blue_AIM120_fired']]

    # sampling time interval
    time_intervel = get_sim_config()[
        "LOG_INTERVAL"] * sample_interval if sample_interval > 0 else get_sim_config()["LOG_INTERVAL"]
    expected_sample_length = int(np.floor(
        sampled_len / sample_interval if sampled_len > 0 else get_sim_config()["MAX_TICK_IN_GAME"]))
    # cut the out last section of the series
    metrics_reduced = metrics_cp
    if sampled_len > 0:
        metrics_reduced = metrics_cp.iloc[:sampled_len]

    # sample every X tick
    if sample_interval > 0:
        metrics_reduced = metrics_reduced.iloc[::sample_interval, :]
        # may not be completely divided or shorter than desired length
        if fill_space:
            shorted_len = len(metrics_reduced)
            if shorted_len < expected_sample_length:
                metrics_reduced = metrics_reduced.append(
                    metrics_reduced.iloc[[-1]*((expected_sample_length)-len(metrics_reduced))], ignore_index=True)
                for j in range(shorted_len, expected_sample_length):
                    metrics_reduced.iloc[j,
                                         0] = metrics_reduced.iloc[j-1, 0] + time_intervel

    if scale_down_time:
        metrics_reduced['time'] = metrics_reduced['time'] / time_intervel

    metrics_reduced = metrics_reduced.reset_index(drop=True)

    for col in normalize_columns:
      metrics_reduced[col] = np.diff(np.insert(metrics_reduced[col].values, 0, metrics_reduced[col].values))


    if save_metric:
        file_path = f"{OUT_PATH}/data/metric_{id}.csv"
        metrics_reduced.to_csv(file_path, index=False)
    return metrics_reduced


def create_standard_scaler(df=None):
    drop_time_df = df.drop('time', axis=1)
    
    # standard scaler for df (with time)
    joblib.dump(StandardScaler().fit(df), f"{OUT_PATH}/standard_scaler.bin")
    # standard scaler for df (WITHOUT time)
    joblib.dump(StandardScaler().fit(drop_time_df),
                f"{OUT_PATH}/standard_scaler_no_time.bin")

    # MinMax Scaler for df (with time)
    joblib.dump(MinMaxScaler().fit(df), f"{OUT_PATH}/minmax_scaler.bin")
    # MinMax Scaler for df (WITHOUT time)
    joblib.dump(MinMaxScaler().fit(drop_time_df),
                f"{OUT_PATH}/minmax_scaler_no_time.bin")

    # MaxAbs Scaler for df (with time)
    joblib.dump(MaxAbsScaler().fit(df), f"{OUT_PATH}/maxabs_scaler.bin")
    # MaxAbs Scaler for df (WITHOUT time)
    joblib.dump(MaxAbsScaler().fit(drop_time_df),
                f"{OUT_PATH}/maxabs_scaler_no_time.bin")

    # Robust Scaler for df (with time)
    joblib.dump(RobustScaler().fit(df), f"{OUT_PATH}/robust_scaler.bin")
    # Robust Scaler for df (WITHOUT time)
    joblib.dump(RobustScaler().fit(drop_time_df),
                f"{OUT_PATH}/robust_scaler_no_time.bin")
    return


def main(config):

    # get path to raw data
    data_set = config["R_Version"]
    global RAW_DATA_PATH
    RAW_DATA_PATH = f'{get_raw_data_path()}/{data_set}/data' if data_set else get_raw_data_path()

    # read config values
    time_sample_interval = config["TIME_ECLAPSE"]
    time_series_length = config["TIME_SERIES_LENGTH"]
    scale_down_time = config["SCALE_DOWN_TIME"]
    filling_empty = config["FILLING_EMPTY"]
    D_Version = config["D_Version"]
    R_Version = config["R_Version"]
    normalize_columns = config["NORMALIZE_COLUMNS"]

    # create directory for clean data if not exist
    global OUT_PATH
    OUT_PATH = os.path.join(DATASET_PATH, D_Version)
    pathlib.Path(OUT_PATH).mkdir(parents=True, exist_ok=True)
    pathlib.Path(f"{OUT_PATH}/data").mkdir(parents=True, exist_ok=True)

    df = None
    # get directory of raw data
    for s in os.listdir(RAW_DATA_PATH):
        p = os.path.join(RAW_DATA_PATH, s)
        # open every simulation and clean those data
        if os.path.isdir(p):
            metrics_reduced = sampling_dataset(id=s, path=p, sampled_len=time_series_length, sample_interval=time_sample_interval,
                                               fill_space=filling_empty, scale_down_time=scale_down_time, normalize_columns=normalize_columns)
            if df is None:
                df = metrics_reduced
            else:
                df = pd.concat([df, metrics_reduced], ignore_index=True)

    # create scalers
    create_standard_scaler(df)

    # copy summary csv
    shutil.copyfile(f"{get_raw_data_path()}/{data_set}/summary.csv", f"{OUT_PATH}/summary.csv")

    # copy critical parameter into info.json 
    json.dump({k:config[k] for k in config if k in ["TIME_ECLAPSE", "TIME_SERIES_LENGTH", "SCALE_DOWN_TIME", "NORMALIZE_COLUMNS"]}, fp=open(f"{OUT_PATH}/info.json", mode="w"))


    print("Baseline - preprocessing - main - end")
