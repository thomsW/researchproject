from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras import Model
from tcn import TCN, tcn_full_summary
import tensorflow as tf
import tensorflow.keras.backend as K
from sklearn.metrics import mean_squared_error
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras import regularizers

def cus_loss_timeless(y_true, y_pred):
    difference = y_pred - y_true
    penalty_r = tf.nn.relu(difference[:,:,0])
    penalty_b = tf.nn.relu(difference[:,:,4])
    squared_difference = tf.square(y_true - y_pred)
    penalty = penalty_r + penalty_b
    return tf.reduce_mean(squared_difference, axis=-1) + penalty * 0.05

def cus_loss(y_true, y_pred):
    difference = y_pred - y_true
    penalty_r = tf.nn.relu(difference[:,:,1])
    penalty_b = tf.nn.relu(difference[:,:,5])
    squared_difference = tf.square(y_true - y_pred)
    penalty = penalty_r + penalty_b
    return tf.reduce_mean(squared_difference, axis=-1) + penalty * 0.05

def create_model(n_step, n_features, n_prediction, is_timeless=True):
    inp = Input(shape=(n_step, n_features))

    tcn_layer = TCN(return_sequences=True )(inp)

    dense1 = Dense(128, activation='relu', bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)
    )(tcn_layer)
    tcn_layer_2 = TCN( )(dense1)
    
    dense2 = Dense(n_features)(tcn_layer_2)

    model = Model(inputs=inp, outputs=dense2)
    if not is_timeless:
        model.compile(loss=cus_loss, optimizer='adam')
    else:
        model.compile(loss=cus_loss_timeless, optimizer='adam')
    model.summary()
    # model.compile(loss='mse', optimizer='adam')
    # tcn_full_summary(model, expand_residual_blocks=False)

    return model