from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import GRU
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras import Model
import tensorflow as tf
import tensorflow.keras.backend as K
from sklearn.metrics import mean_squared_error
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras import regularizers

def cus_loss_timeless(y_true, y_pred):
    difference = y_pred - y_true
    penalty_r = tf.nn.relu(difference[:,:,0])
    penalty_b = tf.nn.relu(difference[:,:,4])
    squared_difference = tf.square(y_true - y_pred)
    penalty = penalty_r + penalty_b
    return tf.reduce_mean(squared_difference, axis=-1) + penalty * 0.05

def cus_loss(y_true, y_pred):
    difference = y_pred - y_true
    penalty_r = tf.nn.relu(difference[:,:,1])
    penalty_b = tf.nn.relu(difference[:,:,5])
    squared_difference = tf.square(y_true - y_pred)
    penalty = penalty_r + penalty_b
    return tf.reduce_mean(squared_difference, axis=-1) + penalty * 0.05

def create_model(n_step, n_features, n_prediction, is_timeless=True):
    inp = Input(shape=(n_step, n_features))

    encoder1 = GRU(128, activation='relu')(inp)
    repeator = RepeatVector(n_prediction)(encoder1)

    decoder1 = GRU(256, activation='relu', return_sequences=True , bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)
    )(repeator)
    dropout1 = Dropout(0.3)(decoder1)
    decoder2 = GRU(512, activation='relu', return_sequences=True, bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)
    )(dropout1)
    dropout2 = Dropout(0.3)(decoder2)
    decoder3 = GRU(256, activation='relu', return_sequences=True, bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)
    )(dropout2)
    dropout3 = Dropout(0.3)(decoder3)

    dense1 = TimeDistributed(Dense(128, activation='relu', bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)
    ))(dropout3)
    dense2 = Dense(n_features)(dense1)

    model = Model(inputs=inp, outputs=dense2)
    if not is_timeless:
        model.compile(loss=cus_loss, optimizer='adam')
    else:
        model.compile(loss=cus_loss_timeless, optimizer='adam')
    model.summary()
    return model