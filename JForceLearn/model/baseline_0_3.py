from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import LSTM, Bidirectional
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras import regularizers

def create_model(n_step, n_features, n_prediction):
# define model
    model = Sequential()
    model.add(LSTM(128, activation='relu', input_shape=(n_step, n_features), return_sequences=True))
    model.add(LSTM(512, activation='relu', return_sequences=True))
    model.add(LSTM(256, activation='relu'))
    model.add(RepeatVector(n_prediction))
    model.add(LSTM(256, activation='relu', return_sequences=True, bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)))
    model.add(Dropout(0.3))
    model.add(LSTM(512, activation='relu', return_sequences=True, bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)))
    model.add(Dropout(0.3))
    model.add(LSTM(128, activation='relu', return_sequences=True, bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)))
    model.add(Dropout(0.3))
    model.add(TimeDistributed(Dense(128, activation='relu', bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4))))
    model.add(TimeDistributed(Dense(n_features
    , bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4)
    )))
    model.compile(loss='mse', optimizer='adam')
    model.summary()
    return model