from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import LSTM, Bidirectional
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras import Model
import tensorflow as tf
import tensorflow.keras.backend as K
from sklearn.metrics import mean_squared_error
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras import regularizers

THERSHOLD = tf.constant(10000,  dtype='float32')

# def customLoss(yTrue,yPred):

#     classes = K.argmax(yTrue)
#     classCount = K.sum(yTrue,axis=0)

#     loss = tf.keras.losses.categorical_crossentropy(yTrue,yPred)

#     val = tf.math.divide_no_nan(loss, K.gather(classCount, classes))
#     return val

def customLoss(red_count, blue_param):
    def loss(yTrue,yPred):
        classes = K.argmax(yTrue)
        classCount = K.sum(yTrue,axis=0)

        loss = tf.keras.losses.categorical_crossentropy(yTrue,yPred)

        # if classes == classCount.shape[0] -1:
        if classes == tf.constant(classCount.shape[0]  -1, dtype='int64'):
            return K.relu( loss * K.gather(red_count, classes) - THERSHOLD)
        else :
            return loss * K.gather(red_count, classes)
    return loss

def create_model(n_step, n_features, n_classes, RED_RESULT_ZERO, BLUE_RESULT_ZERO, is_timeless=True):
    red_param = tf.constant(RED_RESULT_ZERO, dtype='float32')
    blue_param = tf.constant(BLUE_RESULT_ZERO, dtype='float32')
    inp = Input(shape=(n_step, n_features))

    encoder1 = LSTM(128, return_sequences=True)(inp)
    red_lstm = LSTM(256, name='red_lstm', return_sequences=True)(encoder1)
    red_lstm2 = LSTM(512, name='red_lstm_2')(red_lstm)
    red1 = Dense(128, activation='relu', bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4), name='red_dense')(red_lstm2)
    red_out = Dense(n_classes, activation='softmax', name='red_out' )(red1)

    model = Model(inputs=inp, outputs=red_out)
    model.compile(loss=customLoss(red_param, blue_param), optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model