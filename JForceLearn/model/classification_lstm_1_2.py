from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import LSTM, Bidirectional
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras import Model
import tensorflow as tf
import tensorflow.keras.backend as K
from sklearn.metrics import mean_squared_error
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras import regularizers

def customLoss(yTrue,yPred):

    classes = K.argmax(yTrue)
    classCount = K.sum(yTrue,axis=0)

    loss = tf.keras.losses.categorical_crossentropy(yTrue,yPred)

    return tf.math.divide_no_nan(loss, K.gather(classCount, classes))

def create_model(n_step, n_features, n_classes, is_timeless=True):
    inp = Input(shape=(n_step, n_features))

    encoder1 = LSTM(128, return_sequences=True)(inp)
    red_lstm = LSTM(256, name='red_lstm')(encoder1)
    red1 = Dense(128, activation='relu', bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4), name='red_dense')(red_lstm)
    red_out = Dense(n_classes, activation='softmax', name='red_out' )(red1)

    blue_lstm = LSTM(256, name='blue_lstm')(encoder1)
    blue1 = Dense(128, activation='relu', bias_regularizer=regularizers.l2(1e-4), kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4), name='blue_dense' )(blue_lstm)
    blue_out = Dense(n_classes, activation='softmax', name='blue_out' )(blue1)

    model = Model(inputs=inp, outputs=[red_out, blue_out])
    model.compile(loss=customLoss, optimizer='adam', loss_weights=[1,1], metrics=['accuracy'])
    model.summary()
    return model