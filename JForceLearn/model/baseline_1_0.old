from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import LSTM, Bidirectional
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras import Model
import tensorflow as tf
import tensorflow.keras.backend as K
from sklearn.metrics import mean_squared_error
from tensorflow.keras.models import Sequential, load_model

def custom_loss(y_true, y_pred, input_tensor):
    r_0 = tf.nn.relu(y_pred[:, 0, 1:2] - input_tensor[:,-1,1:2])
    r_n = tf.nn.relu(y_pred[:, 1:5, 1:2] - y_pred[:,0:4,1:2])
    r_n = tf.reshape(r_n, [-1, r_n.shape[1]] )
    r = tf.concat([r_0, r_n],1)

    b_0 = tf.nn.relu(y_pred[:, 0, 5:6] - input_tensor[:,-1,5:6])
    b_n = tf.nn.relu(y_pred[:, 1:5, 5:6] - y_pred[:,0:4,5:6])
    b_n = tf.reshape(b_n, [-1, b_n.shape[1]] )
    b = tf.concat([b_0, b_n],1)

    return K.mean(K.square(y_true - y_pred), axis=-1) + r + b

def custom_loss_timeless(y_true, y_pred, input_tensor):
    r_0 = tf.nn.relu(y_pred[:, 0, 0:1] - input_tensor[:,-1,0:1])
    r_n = tf.nn.relu(y_pred[:, 1:5, 0:1] - y_pred[:,0:4,0:1])
    r_n = tf.reshape(r_n, [-1, r_n.shape[1]] )
    r = tf.concat([r_0, r_n],1)

    b_0 = tf.nn.relu(y_pred[:, 0, 4:5] - input_tensor[:,-1,4:5])
    b_n = tf.nn.relu(y_pred[:, 1:5, 4:5] - y_pred[:,0:4,4:5])
    b_n = tf.reshape(b_n, [-1, b_n.shape[1]] )
    b = tf.concat([b_0, b_n],1)

    return K.mean(K.square(y_true - y_pred), axis=-1) + r + b

def create_model(n_step, n_features, n_prediction, is_timeless = False):
    inp = Input(shape=(n_step, n_features))
    tar = Input(shape=(n_prediction, n_features))

    encoder1 = LSTM(128, activation='relu')(inp)
    repeator = RepeatVector(n_prediction)(encoder1)

    decoder1 = LSTM(256, activation='relu', return_sequences=True)(repeator)
    dropout1 = Dropout(0.3)(decoder1)
    decoder2 = LSTM(512, activation='relu', return_sequences=True)(dropout1)
    dropout2 = Dropout(0.3)(decoder2)
    decoder3 = LSTM(256, activation='relu', return_sequences=True)(dropout2)
    dropout3 = Dropout(0.3)(decoder3)

    dense1 = TimeDistributed(Dense(128, activation='relu'))(dropout3)
    dense2 = TimeDistributed(Dense(n_features))(dense1)

    model = Model(inputs=[inp, tar], outputs=dense2)
    if not is_timeless:
        model.add_loss(custom_loss(tar, dense2, inp))
    else:
        model.add_loss(custom_loss_timeless(tar, dense2, inp))
    model.compile(loss=None, optimizer='adam')
    model.summary()
    return model