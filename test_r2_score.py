import pandas as pd
from sklearn.metrics import r2_score

# MODEL = "June2021_baseline_l1_1.2_timeless"
# data_path = f"JForceLearn/server_result/{MODEL}/prediction/estimate/data"

df_arr = []
for id in range(5000):
  df_arr.append(pd.read_csv(f"JForceLearn/preprocessing/server_5000/data/metric_{id}.csv").drop("time", axis=1)[11:])

gt_df = pd.concat(df_arr)


MODEL = "June2021_baseline_l1_1.2_timeless"
for MODEL in ["June2021_baseline_l1_1.2_timeless", "June2021_gru_l1_1.2_timeless", "June2021_tcn_l1_1.2"]:
  data_path = f"JForceLearn/server_result/{MODEL}/prediction/estimate/data"
  df_arr_pred = []
  for id in range(5000):
    df_arr_pred.append(pd.read_csv(f"{data_path}/prediction_{id}.csv").drop("Unnamed: 0", axis=1))
  pred_df = pd.concat(df_arr_pred)

  print(f"{MODEL} R2 score: {r2_score(gt_df.values, pred_df.values)}")
