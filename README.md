# conda
conda envorinment should install tensorflow-gpu version 

# package install
jsonc-parser
joblib
pandas
sklearn
matplotlib
seaborn
keras-tcn --no-dependencies

*note* manual install missing package

# setup config file
please check the config file accordingly,
config files are save in jsonc with can put in C-type comments
for model config detail, please check baseline_0.1.jsonc

# environment variable
Change `env.Mac` file if you using mac
Change `env.Server` file if you running on uni server
Add `enc.Win` file if you using window

# Run preprocessing 
`python main.preprocessing.py --config {config.jsonc} --program {preprocessing.xxx}`

# Run model training
`python main.training.py --config {config.jsonc}`

# run test python script
`python test_{xxx}.py`

# visual studio code
There is a luanch file, you can run debugger when it
for those who are not using it, can check the file for run command

any query send email to wongthomas03@gmail.com