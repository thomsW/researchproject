import os
import sys
import argparse
import importlib
import traceback
from jsonc_parser.parser import JsoncParser

from JForceLearn.helper.helperEnv import get_raw_data_path, get_config_path, get_root_dir, set_root_dir

set_root_dir(os.path.dirname(os.path.abspath(__file__)))

parser = argparse.ArgumentParser()
parser.add_argument("--config", help="path to config file")
parser.add_argument("--program", help="module that use to process data")
args = parser.parse_args()
config = None

try:
    config_path = os.path.join(get_root_dir(), args.config)
    program = args.program

    # load config
    config = JsoncParser.parse_file(config_path)
    # load preprocess function
    fn = importlib.import_module(program)
    fn.main(config)
except Exception as e:
    traceback.print_exc()
    print("Config not found")
