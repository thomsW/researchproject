import pandas as pd
import numpy as np
import collections
from sklearn.semi_supervised import LabelSpreading, LabelPropagation

summary = pd.read_csv('/Users/thomsonwong/UniWork/Year2Sem1/target/JForceLearn/preprocessing/server_5000/summary_2.csv')
summary.describe()



for knn in range(1,8):
# knn  = 7
   max_idx = np.argpartition(summary["red_jets"], -knn).iloc[-knn:]
   min_idx = np.argpartition(summary["red_jets"], knn).iloc[0:knn]
   median = summary["red_jets"][summary["red_jets"] == summary["red_jets"].median()].index[0:knn].values

   semi_label = [-1 for _ in range(len(summary["red_jets"]))]
   for idx in max_idx:
      semi_label[idx] = 2
   for idx in min_idx:
      semi_label[idx] = 0
   for idx in median:
      semi_label[idx] = 1

   model = LabelSpreading(kernel="knn",n_neighbors=knn, max_iter=100000)
   model.fit(summary[["red_jets", "blue_jets"]], semi_label)

   yhat = model.predict(summary[["red_jets", "blue_jets"]])

   print(collections.Counter(yhat))
print("")
'''
           trialID     red_jets    blue_jets
count  5000.000000  5000.000000  5000.000000
mean   2499.500000    17.891200     0.689600
std    1443.520003     6.399278     0.974598
min       0.000000     0.000000     0.000000
25%    1249.750000    13.000000     0.000000
50%    2499.500000    18.000000     0.000000
75%    3749.250000    23.000000     1.000000
max    4999.000000    36.000000     6.000000
'''
len( summary[ summary['red_jets'] < summary['red_jets'].mean() ]['trialID'].tolist())
# mean
# 2230 = 44.6%
len( summary[ summary['red_jets'] < summary['red_jets'].mean() + summary['red_jets'].std() ]['trialID'].tolist())
# mean + std
# 4244 = 84.88%
len( summary[ summary['red_jets'] < summary['red_jets'].mean() + summary['red_jets'].std() * 2 ]['trialID'].tolist())
# mean + 2*std
# 4938 = 98.76%
# June2021_baseline_l1_1.2_timeless R2 score: 0.9910961619338748
# June2021_gru_l1_1.2_timeless R2 score: 0.9910911342849203
# June2021_tcn_l1_1.2 R2 score: 0.07104996914687574
'''
lstm baseline (all mse)
            max mse : {1: 4242, 0: 758}
any mse > threshold : {1: 4242, 0: 758}
   confusion metric : [ [102, 654],
                        [656, 3588]
                      ]
      True_negative : 102
      True_positive : 3588
     False_negative : 656
     False_positive : 654
           accuracy : 0.738
          precision : 0.8458274398868458
             Recall : 0.8454288407163054
False Positive Rate : 0.8650793650793651
           F1 score : 0.845628093330191
'''
'''
lstm baseline (jet only mse)

   confusion metric : [ [63, 693],
                        [445, 3799]
                      ]
      True_negative : 63
      True_positive : 3799
     False_negative : 445
     False_positive : 693
           accuracy : 0.7724
          precision : 0.8457257346393589
             Recall : 0.8951460885956645
False Positive Rate : 0.9166666666666666
           F1 score : 0.8697344322344323
'''
'''
lstm baseline (all L1 error)

   confusion metric : [ [82, 674],
                        [475, 3769]
                      ]
      True_negative : 82
      True_positive : 3769
     False_negative : 475
     False_positive : 674
           accuracy : 0.7702
          precision : 0.8483006977267612
             Recall : 0.8880772855796418
False Positive Rate : 0.8915343915343915
           F1 score : 0.8677333947277541
'''
'''
lstm baseline (jet only L1 error)

   confusion metric : [ [81, 675],
                        [637, 3607]
                      ]
      True_negative : 81
      True_positive : 3607
     False_negative : 637
     False_positive : 675
           accuracy : 0.7376
          precision : 0.8423633815973844
             Recall : 0.8499057492931197
False Positive Rate : 0.8928571428571429
           F1 score : 0.8461177574478066
'''

'''
gru (all mse)

min case:
df[df["0"] ==df["0"].min()]
110         110  0.010301

max case:
df[df["0"] ==df["0"].max()]
27          27  0.390387

            max mse : {1: 4909, 0: 91}
any mse > threshold : {1: 4909, 0: 91}
   confusion metric : [
                        [11, 745],
                        [80, 4164]
                      ]
      True_negative : 11
      True_positive : 4164
     False_negative : 80
     False_positive : 745
           accuracy : 0.835
          precision : 0.8482379303320432
             Recall : 0.9811498586239397
False Positive Rate : 0.9854497354497355
           F1 score : 0.9098656178302196
'''

'''
tcn (all mse)
            max mse : {1: 4976, 0: 24}
any mse > threshold : {1: 4976, 0: 24}
    confusion_metric: [
                        [5, 751],
                        [19, 4225]
                      ]
      True_negative : 5
      True_positive : 4225
     False_negative : 19
     False_positive : 751
           accuracy : 0.846
          precision : 0.8490755627009646
             Recall : 0.9955230914231856
False Positive Rate : 0.9933862433862434
           F1 score : 0.9164859002169197
'''
