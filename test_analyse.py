import numpy as np
import seaborn as sns
import pandas as pd
import collections
####
# Parameter 
##
# mType = "gru"
mType = "lstm"
# mType = "tcn"
#
# error = "MSE"
# error = "MSE-jet"
# error = "MAE"
error = "MAE-jet"
#
###

MODEL = f"June2021_{mType if mType != 'lstm' else 'baseline'}_l1_1.2_timeless"
plt_title = "LSTM max mse frist 120"
time = 120
data_path = f"JForceLearn/server_result/{MODEL}/prediction/estimate/data"

mse_path = f"{data_path}/mean_square_error_xxx.csv"
jet_mse_path = f"{data_path}/jet_mean_square_error_xxx.csv"
l1_error_path = f"{data_path}/l_one_error_xxx.csv"
jet_l1_error_path = f"{data_path}/jet_l_one_error_xxx.csv"

if error == "MSE":
  selected_path = mse_path
elif error == "MSE-jet":
  selected_path = jet_mse_path
elif error == "MAE":
  selected_path = l1_error_path
elif error == "MAE-jet":
  selected_path = jet_l1_error_path


summary = pd.read_csv('/Users/thomsonwong/UniWork/Year2Sem1/target/JForceLearn/preprocessing/server_5000/summary_2.csv')
summary.describe()
summary['label'] = summary['red_jets'] < summary['red_jets'].mean() + summary['red_jets'].std()
# 1 is normal, 0 is outlier
summary['label'] = summary['label'].astype(int)

maximum_mse_array = []
time_array = []
all_mse = []
all_label = []
for id in range(0,5000):
  df = pd.read_csv(selected_path.replace('xxx', f'{id}'))
  if time > 0:
    df = df[0:time]
  mse_arr = abs(df['0'].values)
  maximum_mse_array.append(max(mse_arr))
  time_array.append(np.argmax(mse_arr))
  all_mse.extend(mse_arr)

max_df = pd.DataFrame(maximum_mse_array)

max_df['label'] = max_df[0] < (max_df[0].mean() + max_df[0].std())
# 1 is normal, 0 is outlier
max_df['label'] = max_df['label'].astype(int)
max_df["time"] = time_array
max_df = max_df.rename(columns={ 0: "error"})

# all_df = pd.DataFrame(all_mse)
# all_df.describe()
# threhold = all_df[0].mean() + all_df[0].std()
# for id in range(0, 5000):
#   df = pd.read_csv(selected_path.replace('xxx', f'{id}'))
#   mse_arr = abs(df['0'].values)
#   if any(mse_arr > threhold):
#     all_label.append(0)
#   else :
#     all_label.append(1)

# any_df = pd.DataFrame()
# any_df["error"] = max_df["error"].values
# any_df["label"] = all_label
# any_df["time"] = time_array

# plot hist
# max_df.describe()
# axes = max_df.hist()
# axes[0][0].set_title(f"{plt_title}")
# axes[0][0].set_ylabel("Frequency")
# axes[0][0].set_xlabel("mse value")
axe = sns.scatterplot(data=max_df, x="time", y="error", hue=summary['label'].values, legend=False)
axe.set_title(f" {mType.upper()}: max {error} against time")
axe.set_ylim([-0.1, 5])
confusion_metric_max = [[0, 0],[0, 0]]
confusion_metric_any = [[0, 0],[0, 0]]
######
# Truth, Predict
#                     0, 0    0, 1
#                     1, 0    1, 1
######
for id in range(0,5000):
  confusion_metric_max[summary['label'][id]][max_df['label'][id]] += 1
#   confusion_metric_any[summary['label'][id]][all_label[id]] += 1

# prient result
print(f"type: {MODEL}")
print("confusion_metric_max:")
print(confusion_metric_max)

True_negative = confusion_metric_max[0][0]
True_positive = confusion_metric_max[1][1]
False_negative = confusion_metric_max[1][0]
False_positive = confusion_metric_max[0][1]
accuracy = (confusion_metric_max[0][0] + confusion_metric_max[1][1]) / 5000
precision = confusion_metric_max[1][1]/ (confusion_metric_max[0][1] + confusion_metric_max[1][1])
Recall = confusion_metric_max[1][1]/ (confusion_metric_max[1][0] + confusion_metric_max[1][1])
False_Positive_Rate = confusion_metric_max[0][1]/ (confusion_metric_max[0][1] + confusion_metric_max[0][0])
F1 = 2 * precision * Recall / (precision + Recall)

print(f"Max True_negative : {True_negative}")
print(f"Max True_positive : {True_positive}")
print(f"Max False_negative : {False_negative}")
print(f"Max False_positive : {False_positive}")
print(f"Max accuracy : {accuracy}")
print(f"Max precision : {precision}")
print(f"Max Recall : {Recall}")
print(f"Max False Positive Rate : {False_Positive_Rate}")
print(f"Max F1 score : {F1}")

print("confusion_metric_any:")
# print(confusion_metric_any)

# True_negative = confusion_metric_any[0][0]
# True_positive = confusion_metric_any[1][1]
# False_negative = confusion_metric_any[1][0]
# False_positive = confusion_metric_any[0][1]
# accuracy = (confusion_metric_any[0][0] + confusion_metric_any[1][1]) / 5000
# precision = confusion_metric_any[1][1]/ (confusion_metric_any[0][1] + confusion_metric_any[1][1])
# Recall = confusion_metric_any[1][1]/ (confusion_metric_any[1][0] + confusion_metric_any[1][1])
# False_Positive_Rate = confusion_metric_any[0][1]/ (confusion_metric_any[0][1] + confusion_metric_any[0][0])
# F1 = 2 * precision * Recall / (precision + Recall)

# print(f"Any True_negative : {True_negative}")
# print(f"Any True_positive : {True_positive}")
# print(f"Any False_negative : {False_negative}")
# print(f"Any False_positive : {False_positive}")
# print(f"Any accuracy : {accuracy}")
# print(f"Any precision : {precision}")
# print(f"Any Recall : {Recall}")
# print(f"Any False Positive Rate : {False_Positive_Rate}")
# print(f"Any F1 score : {F1}")

# print("end")