import os
import sys
import argparse
import traceback
import importlib
from jsonc_parser.parser import JsoncParser


from JForceLearn.helper.helperEnv import get_raw_data_path, get_config_path, get_root_dir, set_root_dir


set_root_dir(os.path.dirname(os.path.abspath(__file__)))

parser = argparse.ArgumentParser()
parser.add_argument("--config", help="path to config file")
args = parser.parse_args()
config = None
try:
    config_path = os.path.join(get_root_dir(), args.config)
    config = JsoncParser.parse_file(config_path)
    # load preprocess function
    fn = importlib.import_module(config["T_Version"])
    fn.main(config)
except Exception as e:
    traceback.print_exc()
