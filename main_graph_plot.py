import os
import pickle
import numpy as np
from JForceLearn.helper.helperEnv import get_raw_data_path, get_config_path, get_root_dir, set_root_dir
import JForceLearn.helper.helper as helper
set_root_dir(os.path.dirname(os.path.abspath(__file__)))

id=0
# ptype = "truth"
ptype = "estimate"

true_path = f"/Users/thomsonwong/UniWork/Year2Sem1/target/JForceLearn/preprocessing/June2021_40Tick_130000"
scaler_path = f"/Users/thomsonwong/UniWork/Year2Sem1/target/JForceLearn/preprocessing/June2021_40Tick_130000/standard_scaler.bin"
file_path = f"/Users/thomsonwong/UniWork/Year2Sem1/target/JForceLearn/result/June2021_baseline_1.0/prediction/{ptype}/data/{id}.npy"
columns = ["time", "red_jets", "red_AIM9_fired", "red_AGM88_fired", "red_AIM120_fired", "blue_jets", "blue_AIM9_fired", "blue_AGM88_fired", "blue_AIM120_fired"]

scaler = helper.load_standard_scaler(scaler_path)
df = helper.load_metric(id, true_path)
time_arr=df["time"]

y = df[10:]
y = y[columns]

results = np.load(file=file_path)
helper.split_view(results, df, time_arr=time_arr ,time_step=10, time_eclapse=40)
print("end")