import os
import pickle
import numpy as np
from jsonc_parser.parser import JsoncParser
from tensorflow.keras.models import load_model

from JForceLearn.helper.helperEnv import get_raw_data_path, get_config_path, get_root_dir, set_root_dir
import JForceLearn.helper.helper as helper
from tensorflow.keras import Model

set_root_dir(os.path.dirname(os.path.abspath(__file__)))

id=17
config_path = os.path.join(get_root_dir(), "./JForceLearn/Config/learning/learning.baseline_1.0_timeless_exp.jsonc")
config = JsoncParser.parse_file(config_path)
data_path = os.path.join(
    get_root_dir(), f"JForceLearn/preprocessing/{config['D_Version']}")


# load model
out_path = os.path.join(get_root_dir(), f"JForceLearn/result/{config['OUT_DIR']}")
model_out = f"{out_path}/{config['CHECKPOINT_NAME']}"
model = load_model(model_out)
model_f = Model(model.input[0], model.output)

d_config = JsoncParser.parse_file(f"{data_path}/info.json")
time_eclapse = d_config["TIME_ECLAPSE"]

# load standard scaler
scaler = helper.load_standard_scaler(f"{data_path}/standard_scaler.bin") if "time" in config["COLUMNS"] else helper.load_standard_scaler(f"{data_path}/standard_scaler_no_time.bin")

df = helper.load_metric(id, data_path)
time_arr = df["time"]
df = df[config["COLUMNS"]]
y = df[10:]
y = y[config["COLUMNS"]]

# # estimate
# results = helper.predict_long_time_forecast(model_f, y, scaler, time_step=config["TIMESTEPS"], prediction_size=config["PREDICT_SIZE"], columns=config["COLUMNS"],
#             is_continuous=True, round_pred_to=0, use_future=1 )

# # truth
results = helper.predict_simulation(model_f, y, scaler, time_step=config["TIMESTEPS"], prediction_size=config["PREDICT_SIZE"], columns=config["COLUMNS"],
            is_continuous=True, round_pred_to=0)

helper.split_view(results, df, time_arr=time_arr, time_step=10, time_eclapse=40, divergence=None)
