import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
import json
from JForceLearn.helper.helperEnv import get_raw_data_path

DATA_SET = "June2021"  # optional, depend on how env variable defined

RAW_DATA_PATH = f'{get_raw_data_path()}/{DATA_SET}/data' if DATA_SET is not None else get_raw_data_path()

COLUMNS = ['time', 'red_jets', 'red_AIM9_fired', 'red_AGM88_fired', 'red_AIM120_fired',
           'blue_jets', 'blue_AIM9_fired', 'blue_AGM88_fired', 'blue_AIM120_fired']
TARGET_COLUMNS = ['red_jets', 'blue_jets']
data = []
# # Clean all dataset and concatenate training dataset
for i in range(0, len(os.listdir(RAW_DATA_PATH)) - 1):
    # check dir exist
    if (not os.path.isdir(f'{RAW_DATA_PATH}/{str(i)}')):
        continue

    # Opening JSON file
    file = open(f'{RAW_DATA_PATH}/{str(i)}/jforce_v6a-metrics.json')
    # Return JSON object as a dictionary
    metrics = json.load(file)
    # closing file
    file.close()

    # Read file as DataFrame
    metrics_df = pd.json_normalize(metrics['metrics'])
    metrics_cp = metrics_df[COLUMNS]

    # get last row of Target column
    data = np.append(data, metrics_cp[TARGET_COLUMNS].tail(1).values)

# Save the number or remaining red/blue jets
df = pd.DataFrame(np.reshape(data, (-1, 2)),
                  columns=TARGET_COLUMNS, dtype='int32')
df.to_csv(f'{get_raw_data_path()}/{DATA_SET}/summary.csv', index_label="trialID")
