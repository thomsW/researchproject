#!/bin/bash

MODEL=~/yuan/NarrVisAI/jforce/nv-jforce/jforce_v6a.nlogo
EXPERIMENT=unisa
# for i in {5942..6500}
i=2070
#do
OUTFILE=/data/data1/kalong/simulations/output_${i}.csv
bash ~/yuan/NarrVisAI/jforce/NetLogo\ 6.0.4/netlogo-headless.sh --model ${MODEL} --experiment ${EXPERIMENT} --table ${OUTFILE}
#done
