import os
import pickle
import numpy as np
import tcn as TCN
import pandas as pd
from jsonc_parser.parser import JsoncParser
from tensorflow.keras.models import load_model
from tensorflow.keras import Model

from JForceLearn.helper.helperEnv import get_raw_data_path, get_config_path, get_root_dir, set_root_dir
import JForceLearn.helper.helper as helper
from JForceLearn.model.baseline_1_2 import cus_loss_timeless, cus_loss

set_root_dir(os.path.dirname(os.path.abspath(__file__)))

# id= 412 #412 #127 #4482 #291 #369
# data_path = "/Users/thomsonwong/UniWork/Year2Sem1/target/JForceLearn/preprocessing/server_5000"

id= 205 #76 #61 #62 #17 #80 #89 #95
data_path = "/Users/thomsonwong/UniWork/Year2Sem1/target/JForceLearn/preprocessing/June2021_40Tick_130000"

start_prediction = 0

# model_ver = "lstm_1.2"
model_ver = "baseline_1.2_timeless"

config_path = os.path.join(get_root_dir(), f"./JForceLearn/Config/learning/learning.{model_ver}.jsonc")
config = JsoncParser.parse_file(config_path)
data_path_s = os.path.join(get_root_dir(), f"JForceLearn/preprocessing/{config['D_Version']}")


# load model
out_path = os.path.join(get_root_dir(), f"JForceLearn/result/{config['OUT_DIR']}")
model_out = f"{out_path}/{config['CHECKPOINT_NAME']}"
# model = load_model(model_out, custom_objects={'cus_loss_timeless': cus_loss_timeless})
model = load_model(model_out, custom_objects={'cus_loss_timeless': cus_loss_timeless, 'tcn': TCN})

# model = load_model(model_out, custom_objects={'cus_loss': cus_loss})

# model_f = Model(model.input[0], model.output)
model_f = model

d_config = JsoncParser.parse_file(f"{data_path}/info.json")
time_eclapse = d_config["TIME_ECLAPSE"]

# load standard scaler
scaler = helper.load_standard_scaler(f"{data_path_s}/standard_scaler.bin") if "time" in config["COLUMNS"] else helper.load_standard_scaler(f"{data_path_s}/standard_scaler_no_time.bin")
overlap = config['OVERLAP'] if 'OVERLAP' in config else 0

for id in range(0,5000):
  try:
    title = f"{model_ver} case: {id}"

    # scaler = None

    df = helper.load_metric(id, data_path)
    time_arr = df["time"]
    df = df[config["COLUMNS"]]
    y = df[start_prediction:]
    y = y[config["COLUMNS"]]

    error_obj = None

    # # estimate
    prediction_type = "estimate"
    results, mse, mse_jet, l1e, l1e_jet = helper.mixed_forecast(model_f, y, scaler, time_step=config["TIMESTEPS"], prediction_size=config["PREDICT_SIZE"], columns=config["COLUMNS"],
                is_continuous=True, round_pred_to=0, use_future=1, overlap=overlap, next_n_time=5 )

    # prediction_type = "estimate"
    # results = helper.predict_long_time_forecast(model_f, y, scaler, time_step=config["TIMESTEPS"], prediction_size=config["PREDICT_SIZE"], columns=config["COLUMNS"],
    #             is_continuous=True, round_pred_to=0, use_future=1, overlap=overlap )

    # truth
    # prediction_type = "truth"
    # results, mse_arr_window, mse_w_score, mspe_arr_window, mspe_w_score = helper.predict_simulation(model_f, y, scaler, time_step=config["TIMESTEPS"], prediction_size=config["PREDICT_SIZE"], columns=config["COLUMNS"],
    #             is_continuous=True, divergence_columns=config["MSE_LABELS"], round_pred_to=0, calc_divg=True)
    # error_obj = {
    #   "mse_arr": mse_arr_window,
    #   "mse_score": mse_w_score,
    #   "mspe_arr": mspe_arr_window,
    #   "mspe_score": mspe_w_score
    # }
    # error_obj = None

    save_file_path= f"{out_path}/prediction/{prediction_type}"
    # prediction = pd.DataFrame(results, columns=config["COLUMNS"])
    # prediction.to_csv(f"{save_file_path}/data/prediction_{id}.csv")
    # divergence = pd.DataFrame(mse)
    # divergence.to_csv(f"{save_file_path}/data/mean_square_error_{id}.csv")
    
    divergence2 = pd.DataFrame(mse_jet)
    divergence2.to_csv(f"{save_file_path}/data/jet_mean_square_error_{id}.csv")

    divergence3 = pd.DataFrame(l1e)
    divergence3.to_csv(f"{save_file_path}/data/l_one_error_{id}.csv")

    divergence4 = pd.DataFrame(l1e_jet)
    divergence4.to_csv(f"{save_file_path}/data/jet_l_one_error_{id}.csv")
    # helper.joint_view(results, df, time_arr=time_arr, time_step=10, time_eclapse=40, error_obj=error_obj, title=title, save_file_name=None)
    helper.mix_view(results, df, time_arr=time_arr, time_step=10, time_eclapse=40, start_prediction=start_prediction, mse=mse, title=title, id=id, outpath=save_file_path )
  except:
    print("error")